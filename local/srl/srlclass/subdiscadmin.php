<?php

require_once(__DIR__ . '/../../../config.php');

global $DB;

$id = $_GET['id'];
$mainid = $_GET['main'];
$subdisc = $_POST['subdisc'];

date_default_timezone_set('Asia/Jakarta');
$date = date("YmdHis", time());

$recordtoinsert1 = new stdClass();

$recordtoinsert1->class_id = $id;
$recordtoinsert1->main_disc_id = $mainid;
$recordtoinsert1->name = $USER->username;
$recordtoinsert1->disc = $subdisc;
$recordtoinsert1->date = $date;

$DB->insert_record('local_sub_disc', $recordtoinsert1);

redirect($CFG->wwwroot . '/local/srl/srlclass/admin_forumdiskusi.php?id=' . $id);