<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/evaluasimandiri.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('Evaluasi Mandiri');

 global $DB;

 $id = $_GET['classid'];
 $materials_id = $_GET['materials'];

 $getEval = "SELECT * FROM {local_self_eval_quest} p WHERE p.class_id = :classid AND p.materials_id= :materialsid";
 $paramEval = array('classid' => $id, 'materialsid' => $materials_id);
 $questions = $DB->get_records_sql($getEval, $paramEval);
 $number = 1;

 echo $OUTPUT->header();

?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Evaluasi Mandiri: Tree</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="index.php?id=<?php echo $id?>">Mata Kuliah</a></li>
              <li class="breadcrumb-item pl-0"><a href="rekomendasi.php?id=<?php echo $id?>&materials=<?php echo $materials_id?>">Strategi Belajar</a></li>
              <li aria-current="page" class="breadcrumb-item active">Evaluasi Mandiri</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-4">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-text-nt text-center mt-4 mb-5">Evaluasi Mandiri: Tree</h5>
           <iframe name="selfeval" style="display:none;"></iframe>
           <form class="container-fluid px-5" method="post" target="selfeval">
             <?php 
              foreach($questions as $question) {
                
                echo '
                <table class="table table-borderless table-sm">
                  <thead class="table-light" style="background-color: whitesmoke;">
                    <tr>
                      <th scope="col" style="width: 4%;">' . $number .'.</th>
                      <th scope="col" style="width: 96%;">'. $question->question .'</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'a" value="1">
                        <label class="form-check-label" for="' . $number .'a">A.' . $question->quest_a .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'b" value="2">
                        <label class="form-check-label" for="' . $number .'b">B.' . $question->quest_b .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'c" value="3">
                        <label class="form-check-label" for="' . $number .'c">C.' . $question->quest_c .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'c" value="4">
                        <label class="form-check-label" for="' . $number .'d">D.' . $question->quest_d .'</label>
                      </div></td>
                    </tr>
                  </tbody>
                </table>
                ';
                $number = $number + 1;
              }
             ?>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 54%;">
             </div>
             <div scope="col" style="width: 46%;">
            <button type="submit" class="btn btn-primary float-right mt-1" data-toggle="modal" data-target="#ModalReview"  style="font-weight: bold;" id="selesaiEval">SELESAI</button>
            <div class="modal fade" id="ModalReview" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalReviewLabel" aria-hidden="true" style="display:none;">
              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
              <div class="modal-content">
                  <div class="modal-body text-center">
                    <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                    <div class="d-inline-block" style="color: #EFA30A;"><i class="fas fa-star"></i></div>
                    <div class="small mt-2">Selamat, kamu telah menyelesaikan evaluasi mandiri dengan baik. Kamu dapat memeriksa kembali jawabanmu pada review.</div>
                    <div class="d-grid mt-3">
                      <a href="hasilbelajar.php?id=<?php echo $id ?>" class="btn btn-secondary mr-1" style="font-weight: bold;width: 100px;">LEWATI</a>
                      <a href="reviewevaluasimandiri.php?classid=<?php echo $id; ?>&materials=<?php echo $materials_id; ?>" class="btn btn-primary" style="font-weight: bold;width: 100px;">REVIEW</a> 
                    </div>
                  </div>
              </div>
              </div>
            </div>
            </div>
           </div>
            </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script>
       ($(document).on("click", "#selesaiEval",function(){
         console.log("fjasdlk;fj;lfja");
         <?php 
          if(!empty($_POST ['1'])){

          
            // $id1 = $_GET['classid'];
            // $materials_id1 = $_GET['materials'];
           
            // $getEval1 = "SELECT * FROM {local_self_eval_quest} p WHERE p.class_id = :classid AND p.materials_id= :materialsid";
            // $paramEval1 = array('classid' => $id1, 'materialsid' => $materials_id1);
            // $questions1 = $DB->get_records_sql($getEval1, $paramEval1);

            $number1 = 1;
            $result = 0;
            // $record = new stdClass();
            // $question = reset($questions);
              // $record->user_id = 1;
              // $record->class_id = 1;
              // $record->materials_id = 1;
              // $record->quest_number = 1;
              // $record->user_answer = 1;
              // $record->kunci = 1;

              // if(true) {
              //   $record->flag = 1;
              //   $result = $result + 1;
              // } else {
              //   $record->flag = 0;
              // }
              // $DB->insert_record('local_srl_self_eval_answer', $record);
            foreach($questions as $question) {
              $record = new stdClass();
              $record->user_id = $USER->id;
              $record->class_id = $id;
              $record->materials_id = $materials_id;
              $record->quest_number = $number1;
              $record->user_answer = $_POST[$question->self_eval_quest_id];
              $record->kunci = $question->ans;

              if($_POST[$question->self_eval_quest_id] == $question->ans) {
                $record->flag = 1;
                $result = $result + 1;
              } else {
                $record->flag = 0;
              }
              $DB->insert_record('local_srl_self_eval_answer', $record);
              $number1++;
            }
            $userResult = new stdClass();
            $userResult->class_id = $id;
            $userResult->materials_id = $materials_id;
            $userResult->user_id = $USER->id;
            $nilai = $result * 100 / count($questions);
            $userResult->self_eval_result = $nilai;
            $DB->insert_record('local_srl_self_eval', $userResult);
          }
          ?>
          $('#ModalReview').fadeIn();
       }))
     </script>
   </body>
 </html>

 <?php
 echo $OUTPUT->footer();
 ?>