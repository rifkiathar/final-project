<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/index.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');



 global $DB;

 $id = $_GET['id'];

 $getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
 $paramClass = array('userid' => $id);
 $class = $DB->get_record_sql($getClass, $paramClass);

 $getMainDisc = "SELECT * FROM {local_main_disc} p WHERE p.class_id = :classid";
 $paramMainDisc = array('classid' => $id);
 $DataPercakapan = $DB->get_records_sql($getMainDisc, $paramMainDisc);

 $getTodo = "SELECT * FROM {local_srl_class_eval_task} p WHERE p.class_id = :classid";
 $paramTodo = array('classid' => $id);
 $todo = $DB->get_records_sql($getTodo, $paramTodo);


 echo $OUTPUT->header();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <title>Forum Diskusi</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-4 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="admin.php?id=<?php echo $id ?>">Mata Kuliah</a></li>
              <li aria-current="page" class="breadcrumb-item active">Forum Diskusi</li>
            </ol>
          </nav>
        </div>
      </div>
    <div class="row">
        <div class="card-course w-100 mt-4">
            <div class="row no-gutters">
            <div class="col-md-2>">
                <img src="img/ASD.png" class="card-img-center ml-4 mt-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
            </div>
            <div class="col-md-10">
                <div class="card-body" id="card-body-nm">
                    <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                    <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                    <p class="card-text-course m-1"><?php echo $class->year ?></p>
                    <p class="card-text-course m-1"><?php echo $class->code ?></p>
                </div>
            </div>
            </div>
          </div>
    </div>
    <div class="row">
      <div class="card col-md-12 mt-4">
        <ul class="nav nav-justified">
          <li class="nav-item">
            <a class="nav-link" href="admin.php?id=<?php echo $id ?>">Materi Kuliah</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_evaluasinoonmandiri.php?id=<?php echo $id ?>">Evaluasi Non Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_evaluasimandiri.php?id=<?php echo $id ?>">Evaluasi Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">Forum Diskusi</a>
          </li>
        </ul>
        <div class="container px-5">
           <div class="card w-100 mt-5 mb-5">
               <div class="row no-gutters">
               <div class="col-md-2">
                 <div class="profile-image m-4"><img class="rounded-circle" src="img/user.png" width="75"></div>
               </div>
               <form class="col-md-10 mt-4" action="maindiscadmin.php?id=<?php echo $id?>" method="POST">
                 <div class="mb-3 mr-4">
                   <textarea class="form-control" id="form-forum" rows="3" placeholder="Tulis pertanyaanmu" name="maindisc"></textarea>
                 </div>
                 <button type="submit" class="btn btn-primary float-right mb-4 mr-4" style="font-weight: bold;">KIRIM</button>
               </form>
               </div>
               </div>
               <div class="card w-100 mb-5">
                 <div class="col-md-12">
                   <?php
                    foreach($DataPercakapan as $Percakapan) {
                      echo '<h6 style="color: #EFA30A;" class="mt-3">'. $Percakapan->name .'</h6>';
                      echo '
                      <div class="d-flex flex-row align-items-center text-left comment-top p-2 bg-white border-bottom px-3">
                        <div class="profile-image mt-3 mb-3"><img class="rounded-circle" src="img/user.png" width="75"></div>
                        <div class="d-flex flex-column ml-3">
                          <div class="d-flex flex-row">
                              <h6 style="color: #EFA30A;" class="mt-3">'. $Percakapan->name .'</h6>
                          </div>
                          <div class="d-flex flex-row">
                            <div class="card-text">'. $Percakapan->disc .'</div>
                          </div>
                          <div class="d-flex flex-row align-items-center align-content-center small mt-1 mb-3">'. $Percakapan->date .'</div>
                        </div>
                      </div>
              
                      <form class="d-flex flex-row add-comment-section mt-4 mb-4" action="subdisc.php?id='. $id.'&main='. $Percakapan->main_disc_id .'" method="POST">
                        <img class="img-fluid img-responsive rounded-circle mr-2" src="img/user.png" width="38">
                        <input type="text" class="form-control mr-3" placeholder="Tulis komentar" name="subdisc">
                        <button class="btn btn-primary" type="submit" style="font-weight: bold;">KIRIM</button>
                      </form>
                      ';
                      $getSubDisc = "SELECT * FROM {local_sub_disc} p WHERE p.main_disc_id = :mainid";
                      $paramSubDisc = array('mainid' => $Percakapan->main_disc_id);
                      $DataSubPercakapan = $DB->get_records_sql($getSubDisc, $paramSubDisc);

                      foreach($DataSubPercakapan as $singleSubPercakapan) {
                        echo '
                        <div class="coment-bottom px-2">
                          <div class="d-flex flex-row align-items-center text-left comment-top p-2 border-bottom px-5 ml-5">
                            <div class="profile-image mt-1 mb-3"><img class="rounded-circle" src="img/user.png" width="75"></div>
                            <div class="d-flex flex-column ml-3">
                                <div class="d-flex flex-row">
                                    <h6 style="color: #EFA30A;" class="mt-1">'. $singleSubPercakapan->name .'</h6>
                                </div>
                                <div class="d-flex flex-row">
                                  <div class="card-text">'. $singleSubPercakapan->disc .'</div>
                              </div>
                                <div class="d-flex flex-row align-items-center align-content-center small mt-1 mb-3">'. $singleSubPercakapan->date .'</div>
                            </div>
                          </div>
                        </div>
                        ';

                      }
                    }
                   
                   ?>
                   <!-- <div class="d-flex flex-row align-items-center text-left comment-top p-2 bg-white border-bottom px-3">
                       <div class="profile-image mt-3 mb-3"><img class="rounded-circle" src="img/Joshua.png" width="75"></div>
                       <div class="d-flex flex-column ml-3">
                           <div class="d-flex flex-row">
                               <h6 style="color: #EFA30A;" class="mt-3">Joshua Putra</h6>
                           </div>
                           <div class="d-flex flex-row">
                             <div class="card-text">Apakah ada yang belum dapat kelompok untuk tugas minggu depan?</div>
                         </div>
                           <div class="d-flex flex-row align-items-center align-content-center small mt-1 mb-3">23 April 2021, 10.00</div>
                       </div>
                   </div>
                   <div class="coment-bottom px-2">
                       <div class="d-flex flex-row add-comment-section mt-4 mb-4"><img class="img-fluid img-responsive rounded-circle mr-2" src="img/Gita.png" width="38"><input type="text" class="form-control mr-3" placeholder="Tulis komentar"><button class="btn btn-primary" disabled type="submit" style="font-weight: bold;">KIRIM</button></div>
                       <div class="d-flex flex-row align-items-center text-left comment-top p-2 border-bottom px-5 ml-5">
                         <div class="profile-image mt-1 mb-3"><img class="rounded-circle" src="img/Raviandra.png" width="75"></div>
                         <div class="d-flex flex-column ml-3">
                             <div class="d-flex flex-row">
                                 <h6 style="color: #EFA30A;" class="mt-1">Raviandra</h6>
                             </div>
                             <div class="d-flex flex-row">
                               <div class="card-text">Kelompok kami masih ada slot untuk satu orang.</div>
                           </div>
                             <div class="d-flex flex-row align-items-center align-content-center small mt-1 mb-3">23 April 2021, 11.00</div>
                         </div>
                       </div>
                   </div> -->
                 </div>
             </div>
           </div>
        </div>
    </div>

    <!-- Optional JavaScript-->

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php
 echo $OUTPUT->footer();
?>

