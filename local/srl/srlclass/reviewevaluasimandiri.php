<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/reviewevaluasimandiri.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('Review Evaluasi Mandiri');

 global $DB;

 $id = $_GET['classid'];
 $materials_id = $_GET['materials'];

 $getEval = "SELECT * FROM {local_self_eval_quest} p WHERE p.class_id = :classid AND p.materials_id= :materialsid";
 $paramEval = array('classid' => $id, 'materialsid' => $materials_id);
 $questions = $DB->get_records_sql($getEval, $paramEval);
 $number = 1;

 $getAns = "SELECT * FROM {local_srl_self_eval_answer} p WHERE p.class_id = :classid AND p.materials_id= :materialsid AND p.user_id= :userid";
 $paramAns = array('classid' => $id, 'materialsid' => $materials_id, 'userid' => $USER->id);
 $anss = $DB->get_records_sql($getAns, $paramAns);


 echo $OUTPUT->header();


?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Review Evaluasi Mandiri: Tree</title>
   </head>
   <body>
     <div class="container">
       <div class="row">
         <div class="card mt-3 w-100" style="border-width: 0px !important;">
           <nav aria-label="breadcrumb">
             <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="index.php?id=<?php echo $id?>">Mata Kuliah</a></li>
              <li class="breadcrumb-item pl-0"><a href="rekomendasi.php?id=<?php echo $id?>&materials=<?php echo $materials_id?>">Strategi Belajar</a></li>
              <li aria-current="page" class="breadcrumb-item active">Review Evaluasi Mandiri</li>
             </ol>
           </nav>
         </div>
       </div>
     <div class="row">
         <div class="card w-100 mt-4">
           <div class="card-body">
           <h5 class="card-text-nt text-center mt-4 mb-5">Review Evaluasi Mandiri: Tree</h5>
           <div class="container-fluid px-5">
             <?php 
              foreach($questions as $review) {
                //filter ans by quest_number
                $filtered = array_filter($anss, function($value) use ($number){
                  return $value->quest_number == $number;
                });
                if($review->ans == 1) {
                  $jawaban = 'A. '. $review->quest_a;
                }elseif($review->ans == 2) {
                  $jawaban = 'B. '. $review->quest_b;
                }elseif($review->ans == 3) {
                  $jawaban = 'C. '. $review->quest_c;
                }else {
                  $jawaban = 'D. '.  $review->quest_d;
                }
                echo '
                <table class="table table-borderless table-sm">
                  <thead class="table-light" style="background-color: whitesmoke;">
                    <tr>
                      <th scope="col" style="width: 4%;">'. $number .'.</th>
                      <th scope="col" style="width: 96%;">'. $review->question .'</th>
                    </tr>
                  </thead>
                  <tbody>';
                if(reset($filtered)->user_answer == 1) {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'a" checked>
                        <label class="form-check-label" for="'. $number .'a">A.'. $review->quest_a .'</label>
                      </div></td>
                    </tr>';
                } else {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'a">
                        <label class="form-check-label" for="'. $number .'a">A.'. $review->quest_a .'</label>
                      </div></td>
                    </tr>';
                }

                if(reset($filtered)->user_answer == 2) {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'b" checked>
                        <label class="form-check-label" for="'. $number .'b">B.'. $review->quest_b .'</label>
                      </div></td>
                    </tr>';
                } else {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'b">
                        <label class="form-check-label" for="'. $number .'b">B.'. $review->quest_b .'</label>
                      </div></td>
                    </tr>';
                }
                if(reset($filtered)->user_answer == 3) {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'c" checked>
                        <label class="form-check-label" for="'. $number .'c">C.'. $review->quest_c .'</label>
                      </div></td>
                    </tr>';
                } else {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'c">
                        <label class="form-check-label" for="'. $number .'c">C.'. $review->quest_c .'</label>
                      </div></td>
                    </tr>';
                }
                if(reset($filtered)->user_answer == 4) {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'d" checked>
                        <label class="form-check-label" for="'. $number .'d">D.'. $review->quest_d .'</label>
                      </div></td>
                    </tr>
                  </tbody>
                </table>
                ';
                } else {
                  echo '
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" disabled type="radio" name="'. $review->self_eval_quest_id .'" id="'. $number .'d">
                        <label class="form-check-label" for="'. $number .'d">D.'. $review->quest_d .'</label>
                      </div></td>
                    </tr>
                  </tbody>
                </table>
                ';
                }
                if(reset($filtered)->flag == 1) {
                  echo '<div class="alert alert-success" role="alert">
                  Jawaban yang tepat: '. $jawaban .'
                </div>';
                } else {
                  echo '<div class="alert alert-danger" role="alert">
                  Jawaban yang tepat: '. $jawaban .'
                </div>';
                }
                
              $number = $number + 1;
              }
             ?>
             
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 54%;">
             
             </div>
             <div scope="col" style="width: 46%;">
             <a href="hasilbelajar.php?id=<?php echo $id ?>"><button type="submit" class="btn btn-primary float-right mt-1" style="font-weight: bold;">SELESAI</button></a>
             
             </div>
           </div>
           </div>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script>
       $("#radio_1").prop("checked", true);
     </script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();
 ?>