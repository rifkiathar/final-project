<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/hasilbelajar.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 global $DB;

 $id = $_GET['id'];

 $getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
 $paramClass = array('userid' => $id);
 $class = $DB->get_record_sql($getClass, $paramClass);

 $getSelfResult = "SELECT * FROM {local_srl_self_eval} p WHERE p.class_id = :classid AND user_id = :userid";
 $paramSelfResult = array('classid' => $id, 'userid' => $USER->id);
 $selfResult = $DB->get_records_sql($getSelfResult, $paramSelfResult);

 $getMaterials = "SELECT * FROM {local_srl_class_materials} p WHERE p.class_id = :classid";
 $paramMaterials = array('classid' => $id);
 $materials = $DB->get_records_sql($getMaterials, $paramMaterials);

 if((count($selfResult) == 0 && count($materials) != 0) || (count($selfResult) != 0 && count($materials) == 0)) {
  $progress = 0;
  $kumulatif = 0;
 } else {
  $progress = count($selfResult) / count($materials) * 100;
  $kumulatif = array_sum(array_column($selfResult, 'self_eval_result')) / count($selfResult);
 }
 
 
 $getTodo = "SELECT * FROM {local_srl_class_eval_task} p WHERE p.class_id = :classid";
 $paramTodo = array('classid' => $id);
 $todo = $DB->get_records_sql($getTodo, $paramTodo);

 echo $OUTPUT->header();

 /* 
 $hasilBelajar = getHasilBelajar();
 $totalMateriDipelajari = $hasilBelajar->total_materi_dipelajari;
 $totalMateri = $hasilBelajar.size;
 $ProgressBelajar = $totalMateriDipelajari / $totalMateri * 100;

 //Nilai Kumulatif
 $nilaiKumulatif = sum(hasilBelajar->nilai)/$totalMateriDipelajari;

 //Capaian Pembelajaran
 for($hasilBelajar as $singleBelajar) {
   $materi = $singleBelajar->materi;
   $tanggal = $singleBelajar->tanggalBelajar;
   $nilai = $singleBelajar->nilai;
 }

 //To Do list
 $todolist = getToDoList();
 for($todolist as $todo) {
   $namaTodo = $todo->nama;
   $tanggal = $todo->tanggal;
   $link = $todo->link;
 }
 */



?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Hasil Belajar: Algoritme dan Struktur Data</title>
   </head>
   <body>
     <div class="container">
       <div class="row">
         <div class="card mt-3 w-100" style="border-width: 0px !important;">
           <nav aria-label="breadcrumb">
             <ol class="breadcrumb breadcrumb-arrow p-0">
               <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
               <li class="breadcrumb-item pl-0"><a href="index.php?id=<?php echo $id?>">Mata Kuliah</a></li>
               <li aria-current="page" class="breadcrumb-item active">Hasil Belajar</li>
             </ol>
           </nav>
         </div>
       </div>
       <div class="row">
         <div class="card-course w-100 mt-4">
             <div class="row no-gutters">
             <div class="col-md-2>">
                 <img src="img/ASD.png" class="card-img-center mt-4 ml-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
             </div>
             <div class="col-md-10">
             <div class="card-body" id="card-body-nm">
                <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                <p class="card-text-course m-1"><?php echo $class->year ?></p>
                <p class="card-text-course m-1"><?php echo $class->desc ?></p>
              </div>
             </div>
             </div>
           </div>
       </div>
     <div class="row">
       <div class="card w-75 mt-4">
         <ul class="nav nav-justified">
           <li class="nav-item">
             <a class="nav-link" href="index.php?id=<?php echo $id ?>">Materi Kuliah</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="evaluasinonmandiri.php?id=<?php echo $id ?>">Evaluasi Non Mandiri</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="forumdiskusi.php?id=<?php echo $id ?>">Forum Diskusi</a>
           </li>
           <li class="nav-item">
             <a class="nav-link active" href="#">Hasil Belajar</a>
           </li>
         </ul>
       <div class="container px-5">
           <div class="card w-100 mt-5">
             <div class="card-body" id="card-body-nm">
             <h5 class="card-text-nt">Status Belajar</h5>
             <div class="d-inline-flex w-100 mb-3 mt-3">
               <div scope="col" style="width: 18%;">
                 <p class="card-text">Progres Belajar</p>
               </div>
               <div scope="col" style="width: 82%;">
                 <div class="progress" style="height:22px;">
                   <div class="progress-bar" style="width:<?php echo $progress?>%;height:22px;background-color:#3CADC9;font-weight: bold;"><?php echo $progress?></div>
                 </div>
               </div>
             </div>
             <div class="d-inline-flex w-100 mb-3 mt-2">
               <div scope="col" style="width: 18%;">
                 <p class="card-text">Nilai Kumulatif</p>
               </div>
               <div scope="col" style="width: 82%;">
                 <div class="progress" style="height:22px;">
                   <div class="progress-bar" style="width:<?php echo $kumulatif?>%;height: 22px;background-color:#3CADC9;font-weight: bold;"><?php echo $kumulatif ?></div>
                 </div>
               </div>
             </div>
             </div>
           </div>
           <div class="card w-100 mt-5 mb-5">
             <div class="card-body" id="card-body-nm">
             <h5 class="card-text-nt">Capaian Pembelajaran</h5>
             <ul class="list-group mt-4">
               <?php
               if(count($selfResult) == 0) {
                 echo '<p class="card-text">Belum ada materi yang dipelajari</p>';
               }
                foreach($selfResult as $singleResult) {
                  $materialid = $singleResult->materials_id;
                  $filtered = array_filter($materials, function($value) use ($materialid){
                    return $value->materials_id == $materialid;
                  });
                  echo '
                  <li class="list-group-item" id="list-group-item-nh">
                    <span class="d-inline-block" style="font-weight: bold;width: 60%;">'. reset($filtered)->judul .'</span>
                    <span class="d-inline-block" style="width: 3%;color: #EFA30A;"><i class="fas fa-star"></i></span>
                    <span class="d-inline-block" style="color: #EFA30A;font-weight: bold;">'. $singleResult->self_eval_result .'</span>
                  </li>
                  ';
                }
               ?>
             </ul>
             </div>
             </div>
           </div>
         </div>
       <div class="col-md-3">
         <div class="card mt-4 ml-3">
           <div class="card-body" id="card-body-nm">
             <h5 class="card-title m-2">To-Do List</h5>
             <div class="list-group mt-4 m-1">
                <?php 
                  foreach($todo as $singleTodo) {
                    echo '
                    <a href="#" class="list-group-item list-group-item-action" id="list-group-item-course">
                      <p class="mb-1">'. $singleTodo->title .'</p>
                      <small>'. $singleTodo->due_date .'</small>
                    </a>
                    ';
                  }
                ?>
             </div>
           </div>
           </div>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();
 ?>