<?php

require_once(__DIR__ . '/../../../config.php');

global $DB;


$id = $_GET['id'];
$title = $_POST['judul'];
$due_date = $_POST['duedate'];
$deskripsi = $_POST['deskripsi'];
$add_file = "";

$recordtoinsert = new stdClass();
$recordtoinsert->class_id = $id;
$recordtoinsert->title = $title;
$recordtoinsert->due_date = $due_date;
$recordtoinsert->deskripsi = $deskripsi;
$recordtoinsert->add_file = $add_file;

$DB->insert_record('local_srl_class_eval_task', $recordtoinsert);

// echo json_encode($recordtoinsert);

redirect($CFG->wwwroot . '/local/srl/srlclass/admin_evaluasinoonmandiri.php?id=' . $id);

