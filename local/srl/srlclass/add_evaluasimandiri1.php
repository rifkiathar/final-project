<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/index.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');


 $id = $_GET['id'];
 $materials_id = $_GET['material'];

 $getMaterials = "SELECT * FROM {local_srl_class_materials} p WHERE p.class_id = :classid AND p.materials_id = :materialid";
 $paramMaterials = array('classid' => $id, 'materialid' => $materials_id);
 $materials = $DB->get_records_sql($getMaterials, $paramMaterials);

 $material = reset($materials);

 echo $OUTPUT->header();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Tambah Evaluasi Mandiri</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="admin.php?id=<?php echo $id ?>">Mata Kuliah</a></li>
              <li class="breadcrumb-item pl-0"><a href="admin_evaluasimandiri.php?id=<?php echo $id ?>">Evaluasi Mandiri</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tambah Evaluasi Mandiri</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="card-course w-100 mt-4">
            <div class="row no-gutters">
            <div class="col-md-2>">
                <img src="img/ASD.png" class="card-img-center mt-4 ml-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
            </div>
            <div class="col-md-10">
                <div class="card-body" id="card-body-nm">
                    <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                    <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                    <p class="card-text-course m-1"><?php echo $class->year ?></p>
                    <p class="card-text-course m-1"><?php echo $class->code ?></p>
                </div>
            </div>
            </div>
          </div>
      </div>
      <div class="row mt-4">
      <div class="card col-md-12">
        <form class="card-body" id="card-body-nm" action ="uploadevalmandiri.php?id=<?php echo $id ?>&materials_id=<?php echo $materials_id ?>"  method="POST" >
          <h5 class="card-title">Tambah Evaluasi Mandiri: <?php echo $material->judul ?></h5>
              <?php 

                for($number = 1;$number <=10;$number++){
                  echo '
                    <div class="d-flex" style="background-color: #f9f9f9;">
                      <div class="card-body" id="card-body-nm">
                        <div class="row mb-2">
                          <label for="inputEvalM'. $number.'" class="col-sm-2 col-form-label">Soal '. $number.'</label>
                          <div class="col-sm-10">
                            <input type="text" class="form-control form-control-sm" id="inputEvalM'. $number.'" name="question'. $number.'">
                          </div>
                        </div>
                        <div class="row mb-2">
                          <label for="inputEvalM'. $number.'OpsiA" class="col-sm-2 col-form-label">Opsi a</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" id="inputEvalM'. $number.'OpsiA" name="questiona'. $number.'">
                          </div>
                          <label for="inputEvalM'. $number.'OpsiAnilai" class="col-sm-2 col-form-label">Kunci Jawaban</label>
                          <div class="col-sm-2 mt-1">
                            <div class="form-check form-check-inline" id="inputEvalM'. $number.'OpsiAnilai">
                              <input class="form-check-input" type="radio" name="kunci'. $number.'" id="EvalM'. $number.'OpsiAnilai100" value="1">
                              <label class="form-check-label" for="EvalM'. $number.'OpsiAnilai100">a</label>
                            </div>
                          </div>
                        </div>
                        <div class="row mb-2">
                          <label for="inputEvalM'. $number.'OpsiB" class="col-sm-2 col-form-label">Opsi b</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" id="inputEvalM'. $number.'OpsiB" name="questionb'. $number.'">
                          </div>
                          <label for="inputEvalM'. $number.'OpsiBnilai" class="col-sm-2 col-form-label">Kunci Jawaban</label>
                          <div class="col-sm-2 mt-1">
                            <div class="form-check form-check-inline" id="inputEvalM'. $number.'OpsiBnilai">
                              <input class="form-check-input" type="radio" name="kunci'. $number.'" id="EvalM'. $number.'OpsiBnilai100" value="2">
                              <label class="form-check-label" for="EvalM'. $number.'OpsiBnilai100">b</label>
                            </div>
                          </div>
                        </div>
                        <div class="row mb-2">
                          <label for="inputEvalM'. $number.'OpsiC" class="col-sm-2 col-form-label">Opsi c</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" id="inputEvalM'. $number.'OpsiC" name="questionc'. $number.'">
                          </div>
                          <label for="inputEvalM'. $number.'OpsiCnilai" class="col-sm-2 col-form-label">Kunci Jawaban</label>
                          <div class="col-sm-2 mt-1">
                            <div class="form-check form-check-inline" id="inputEvalM'. $number.'OpsiCnilai">
                              <input class="form-check-input" type="radio" name="kunci'. $number.'" id="EvalM'. $number.'OpsiCnilai100" value="3">
                              <label class="form-check-label" for="EvalM'. $number.'OpsiCnilai100">c</label>
                            </div>
                          </div>
                        </div>
                        <div class="row mb-2">
                          <label for="inputEvalM'. $number.'OpsiD" class="col-sm-2 col-form-label">Opsi d</label>
                          <div class="col-sm-6">
                            <input type="text" class="form-control form-control-sm" id="inputEvalM'. $number.'OpsiD" name="questiond'. $number.'">
                          </div>
                          <label for="inputEvalM'. $number.'OpsiDnilai" class="col-sm-2 col-form-label">Kunci Jawaban</label>
                          <div class="col-sm-2 mt-1">
                            <div class="form-check form-check-inline" id="inputEvalM'. $number.'OpsiDnilai">
                              <input class="form-check-input" type="radio" name="kunci'. $number.'" id="EvalM'. $number.'OpsiDnilai100" value="4">
                              <label class="form-check-label" for="EvalM'. $number.'OpsiDnilai100">d</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ';
                }
              
              ?>
            <div class="d-inline-flex w-100 mt-3">
              <div scope="col" style="width: 54%;">
                
              </div>
              <div scope="col" style="width: 46%;">
              <button type="submit" name="submit" class="btn btn-primary float-right" style="font-weight: bold;" id="simpanEvalM" >SIMPAN</button>
              <button class="btn btn-secondary float-right mr-2" style="font-weight: bold; z-index:5;" id="batalEvalM">BATAL</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>

    <!-- Optional JavaScript; choose one of the two! -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
  </body>
</html>

<?php
 echo $OUTPUT->footer();
?>

