<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/index.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 $id = $_GET['id'];

 $getLo = "SELECT * FROM {local_class_lo} p WHERE p.class_id = :userid";
 $paramLo = array('userid' => $id);
 $lo = $DB->get_records_sql($getLo, $paramLo);

 $getTodo = "SELECT * FROM {local_srl_class_eval_task} p WHERE p.class_id = :classid";
 $paramTodo = array('classid' => $id);
 $todo = $DB->get_records_sql($getTodo, $paramTodo);


 echo $OUTPUT->header();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <title>Evaluasi Non Mandiri: Algoritme dan Struktur Data</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="admin.php?id=<?php echo $id ?>">Mata Kuliah</a></li>
              <li aria-current="page" class="breadcrumb-item active">Evaluasi Non Mandiri</li>
            </ol>
          </nav>
        </div>
      </div>
    <div class="row">
        <div class="card-course w-100 mt-4">
            <div class="row no-gutters">
            <div class="col-md-2>">
                <img src="img/ASD.png" class="card-img-center ml-4 mt-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
            </div>
            <div class="col-md-10">
                <div class="card-body" id="card-body-nm">
                    <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                    <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                    <p class="card-text-course m-1"><?php echo $class->year ?></p>
                    <p class="card-text-course m-1"><?php echo $class->code ?></p>
                </div>
            </div>
            </div>
          </div>
    </div>
    <div class="row">
      <div class="card col-sm-12 mt-4">
        <ul class="nav nav-justified">
          <li class="nav-item">
            <a class="nav-link" href="admin.php?id=<?php echo $id ?>">Materi Kuliah</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">Evaluasi Non Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_evaluasimandiri.php?id=<?php echo $id ?>">Evaluasi Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_forumdiskusi.php?id=<?php echo $id ?>">Forum Diskusi</a>
          </li>
        </ul>
      <div class="container px-5">
        <a class="float-right mt-3 mb-4"><i class="far fa-plus-square fa-2x" style="color: #28527A;" data-toggle="modal" data-toggle="tooltip" data-target="#ModalAddEvalNM" data-placement="top" title="Tambah"></i></a>
        <form action="unggahevaladmin.php?id=<?php echo $id ?>" method="post">
        <div class="modal fade" id="ModalAddEvalNM" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalAddEvalNMLabel" aria-hidden="true">
          <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body">
              <h5 class="modal-title mt-2 mb-3 text-center" id="ModalAddEvalNMLabel" style="color: #EFA30A;">Tambah Evaluasi Non Mandiri</h5>
            <form style="color: black; font-size: 16px;" action="unggahevaladmin.php?id='. $id .'" method="POST">
                <div class="row mb-3">
                  <label for="inputJudulEvalNM" class="col-sm-3 col-form-label">Judul</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="inputJudulEvalNM" name="judul">
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="TextareaDescEvalNM" class="col-sm-3 form-label">Deskripsi</label>
                  <div class="col-sm-9">
                    <textarea class="form-control" id="TextareaDescEvalNM" rows="2" name="deskripsi"></textarea>
                  </div>
                </div>
                <div class="row mb-3">
                  <label for="DateEvalNM" class="col-sm-3 form-label">Due Date</label>
                  <div class="col-sm-9">
                    <input class="form-control" type="datetime-local" id="DateEvalNM" name="duedate">
                  </div>
                </div>
                <!-- <div class="row mb-3">
                  <label for="FileEvalNM" class="col-sm-3 form-label">File Tambahan</label>
                  <div class="col-sm-9">
                    <input type="file" class="form-control" accept=".ppt,.pptx,.pdf" id="FileEvalNM">
                  </div>
                </div> -->
          </form>
              <div class="d-grid text-right">
                <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                <button type="submit" class="btn btn-primary" style="font-weight: bold;width: 100px;">SIMPAN</button> 
              </div>
            </div>
            </div>
          </div>
        </div>
        </form>
        <div class="card w-100 mt-5 mb-5">
          <?php
            foreach($todo as $singletodo) {
              echo '
              <div class="row">
                <div class="col-md-12">
                  <div class="card-body" id="card-body-nm">
                    <h5 class="card-text-nt mb-3">'. $singletodo->judul .'</h5>
                    <p class="card-text">'. $singletodo->deskripsi .'</p>
                    <p class="card-text mt-3" style="font-weight: bold;">Due date: '. $singletodo->due_date.'</p>
                    
                    <a href="#" class="float-right mt-3 mb-3 mr-3"><i class="far fa-edit fa-2x" style="color: #28527A;" data-toggle="modal" data-target="#ModalEditEvalNM"data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                    <div class="modal fade" id="ModalEditEvalNM" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalEditEvalNMLabel" aria-hidden="true">
                      <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-body">
                          <h5 class="modal-title mt-2 mb-3 text-center" id="ModalEditEvalNMLabel" style="color: #EFA30A;">Edit Evaluasi Non Mandiri</h5>
                        <form style="color: black; font-size: 16px;">
                            <div class="row mb-3">
                              <label for="inputEditJudulEvalNM" class="col-sm-3 col-form-label">Judul</label>
                              <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputEditJudulEvalNM">
                              </div>
                            </div>
                            <div class="row mb-3">
                              <label for="TextareaEditDescEvalNM" class="col-sm-3 form-label">Deskripsi</label>
                              <div class="col-sm-9">
                                <textarea class="form-control" id="TextareaEditDescEvalNM" rows="2"></textarea>
                              </div>
                            </div>
                            <div class="row mb-3">
                              <label for="DateEditEvalNM" class="col-sm-3 form-label">Due Date</label>
                              <div class="col-sm-9">
                                <input class="form-control" type="datetime-local" id="DateEditEvalNM">
                              </div>
                            </div>
                            <div class="row mb-3">
                              <label for="FileEditEvalNM" class="col-sm-3 form-label">File Tambahan</label>
                              <div class="col-sm-9">
                                <input type="file" class="form-control" accept=".ppt,.pptx,.pdf" id="FileEditEvalNM">
                              </div>
                            </div>
                      </form>
                          <div class="d-grid text-right">
                            <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                            <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">SIMPAN</a> 
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                    <a href="#" class="float-right mt-3 mb-3 mr-3"><i class="far fa-trash-alt fa-2x" style="color: red;" data-toggle="modal" data-target="#ModalHapusEvalNM" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
                    <div class="modal fade" id="ModalHapusEvalNM" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalHapusEvalNMLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
                    <div class="modal-content">
                      <div class="modal-body text-center">
                        <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                        <h6 class="modal-title mt-2" id="ModalHapusEvalNMLabel">Apakah kamu yakin akan menghapus evaluasi non mandiri ini?</h6>
                        <div class="d-grid mt-3">
                          <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">TIDAK</button>
                          <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">YA</a> 
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  </div>
                </div>
              </div>
              ';
            }
          ?>

          <!-- <div class="row">
          <div class="col-md-12">
            <div class="card-body" id="card-body-nm">
              <h5 class="card-text-nt mb-3">Tree</h5>
              <p class="card-text">Buatlah program Stack dengan bahasa C. Format berisi source code dan screenshot program.</p>
              <p class="card-text mt-3" style="font-weight: bold;">Due date: 30 April 2021, 23:59</p>
              
              <a href="#" class="float-right mt-3 mb-3 mr-3"><i class="far fa-edit fa-2x" style="color: #28527A;" data-toggle="modal" data-target="#ModalEditEvalNM"data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
              <div class="modal fade" id="ModalEditEvalNM" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalEditEvalNMLabel" aria-hidden="true">
                <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-body">
                    <h5 class="modal-title mt-2 mb-3 text-center" id="ModalEditEvalNMLabel" style="color: #EFA30A;">Edit Evaluasi Non Mandiri</h5>
                  <form style="color: black; font-size: 16px;">
                      <div class="row mb-3">
                        <label for="inputEditJudulEvalNM" class="col-sm-3 col-form-label">Judul</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control" id="inputEditJudulEvalNM">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="TextareaEditDescEvalNM" class="col-sm-3 form-label">Deskripsi</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="TextareaEditDescEvalNM" rows="2"></textarea>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="DateEditEvalNM" class="col-sm-3 form-label">Due Date</label>
                        <div class="col-sm-9">
                          <input class="form-control" type="datetime-local" id="DateEditEvalNM">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="FileEditEvalNM" class="col-sm-3 form-label">File Tambahan</label>
                        <div class="col-sm-9">
                          <input type="file" class="form-control" accept=".ppt,.pptx,.pdf" id="FileEditEvalNM">
                        </div>
                      </div>
                </form>
                    <div class="d-grid text-right">
                      <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                      <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">SIMPAN</a> 
                    </div>
                  </div>
                  </div>
                </div>
              </div>
              <a href="#" class="float-right mt-3 mb-3 mr-3"><i class="far fa-trash-alt fa-2x" style="color: red;" data-toggle="modal" data-target="#ModalHapusEvalNM" data-toggle="tooltip" data-placement="top" title="Hapus"></i></a>
              <div class="modal fade" id="ModalHapusEvalNM" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalHapusEvalNMLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
              <div class="modal-content">
                <div class="modal-body text-center">
                  <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                  <h6 class="modal-title mt-2" id="ModalHapusEvalNMLabel">Apakah kamu yakin akan menghapus evaluasi non mandiri ini?</h6>
                  <div class="d-grid mt-3">
                    <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">TIDAK</button>
                    <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">YA</a> 
                  </div>
                </div>
              </div>
            </div>
            </div>
            </div>
          </div>
        </div> -->

        </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Optional JavaScript-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php
 echo $OUTPUT->footer();
?>

