<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/index.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 $id = $_GET['id'];

 $getLo = "SELECT * FROM {local_class_lo} p WHERE p.class_id = :userid";
 $paramLo = array('userid' => $id);
 $lo = $DB->get_records_sql($getLo, $paramLo);

 $getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
 $paramClass = array('userid' => $id);
 $class = $DB->get_record_sql($getClass, $paramLo);

 $getMaterials = "SELECT * FROM {local_srl_class_materials} p WHERE p.class_id = :classid";
 $paramMaterials = array('classid' => $id);
 $materials = $DB->get_records_sql($getMaterials, $paramMaterials);

 echo $OUTPUT->header();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <title>Course: Algoritme dan Struktur Data</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Mata Kuliah</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="card-course w-100 mt-4">
            <div class="row no-gutters">
            <div class="col-md-2>">
                <img src="img/ASD.png" class="card-img-center mt-4 ml-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
            </div>
            <div class="col-md-10">
                <div class="card-body" id="card-body-nm">
                    <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                    <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                    <p class="card-text-course m-1"><?php echo $class->year ?></p>
                    <p class="card-text-course m-1"><?php echo $class->code ?></p>
                </div>
            </div>
            </div>
          </div>
      </div>
    <div class="row">
      <div class="card w-100 mt-4">
        <ul class="nav nav-justified">
          <li class="nav-item">
            <a class="nav-link active" href="#">Materi Kuliah</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_evaluasinoonmandiri.php?id=<?php echo $id ?>">Evaluasi Non Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_evaluasimandiri.php?id=<?php echo $id ?>">Evaluasi Mandiri</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="admin_forumdiskusi.php?id=<?php echo $id ?>">Forum Diskusi</a>
          </li>
        </ul>
      <div class="container px-5">
            <table class="table table-striped mt-5">
              <thead class="table-dark">
              <tr>
                  <th scope="col"></th>
                  <th scope="col">Learning Outcomes
                    <a href="#" class="float-right"><i class="far fa-edit fa-lg" style="color: white;" data-toggle="modal" data-toggle="tooltip" data-target="#ModalEditLO" data-placement="top" title="Edit"></i></a>
                    <div class="modal fade" id="ModalEditLO" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalEditLOLabel" aria-hidden="true">
                      <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
                      <form class="modal-content" action="upload_lo.php?id=<?php echo $id ?>" method="POST">
                        <div class="modal-body">
                          <h5 class="modal-title mt-2 mb-3 text-center" id="ModalEditLOLabel" style="color: #EFA30A;">Edit Learning Outcomes</h5>
                        <form style="color: black; font-size: 16px;">
                        <div class="row mb-3">
                          <label for="inputEditLO1" class="col-sm-1 col-form-label" style="color:black;">LO1</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO1" name="lo1">
                          </div>
                        </div>
                        <div class="row mb-3">
                          <label for="inputEditLO2" class="col-sm-1 col-form-label" style="color:black;">LO2</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO2" name="lo2">
                          </div>
                        </div>
                        <div class="row mb-3">
                          <label for="inputEditLO3" class="col-sm-1 col-form-label" style="color:black;">LO3</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO3" name="lo3">
                          </div>
                        </div>
                        <div class="row mb-3">
                          <label for="inputEditLO4" class="col-sm-1 col-form-label" style="color:black;">LO4</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO4" name="lo4">
                          </div>
                        </div>
                        <div class="row mb-3">
                          <label for="inputEditLO5" class="col-sm-1 col-form-label" style="color:black;">LO5</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO5" name="lo5">
                          </div>
                        </div>
                        <div class="row mb-3">
                          <label for="inputEditLO6" class="col-sm-1 col-form-label" style="color:black;">LO6</label>
                          <div class="col-sm-11">
                            <input type="text" class="form-control" id="inputEditLO6" name="lo6">
                          </div>
                        </div>
                      </form>
                          <div class="d-grid text-right">
                            <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                            <button class="btn btn-primary" style="font-weight: bold;width: 100px;" name="id" value="<?php $id ?>">SIMPAN</button> 
                          </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </th>
                </tr>
              </thead>
              <tbody>
              <?php
                  foreach($lo as $singleLO) {
                ?>
                <tr>
                  <td scope="row"><?php echo $singleLO->lo_id ?></td>
                  <td><?php echo $singleLO->lo ?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
            </table>
        <h5 class="card-title mt-3">Materi Kuliah
          <a href="#" class="float-right"><i class="far fa-plus-square fa-lg" style="color: #28527A;" data-toggle="modal" data-toggle="tooltip" data-target="#ModalAddMateri" data-placement="top" title="Tambah"></i></a>
          </h5>
          <div class="modal fade" id="ModalAddMateri" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalAddMateriLabel" aria-hidden="true">
            <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
            <form class="modal-content" action="process.php?id=<?php echo $id?>" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <h5 class="modal-title mt-2 mb-3 text-center" id="ModalAddMateriLabel" style="color: #EFA30A;">Tambah Materi</h5>
              <form style="color: black; font-size: 16px;">
              <div class="row mb-3">
                <label for="inputJudulMateri" class="col-sm-3 col-form-label">Judul</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" id="inputJudulMateri" name="judul">
                </div>
              </div>
              <div class="row mb-3">
                <label for="TextareaDescMateri" class="col-sm-3 form-label">Deskripsi</label>
                <div class="col-sm-9">
                  <textarea class="form-control" type="text" id="TextareaDescMateri" rows="2" name = desc></textarea>
                </div>
              </div>
              <div class="row mb-3">
                <label for="formImgMateri" class="col-sm-3 form-label">Gambar Materi</label>  
                <div class="col-sm-9">
                  <input class="form-control" type="file" accept=".jpg,.gif,.png,.svg" id="formImgMateri" name="gambar_materi">
                </div>
              </div>
              <div class="row mb-3">
                <label for="formFileVideo" class="col-sm-3 form-label">Control Video</label>  
                <div class="input-group col-sm-9">
                  <input type="text" aria-label="Link" class="form-control" id="formFileVideo" placeholder="Link Youtube" aria-describedby="FileVideoHelp" name="link">
                </div>
                <div class="col-sm-3"></div>
                <div id="FileVideoHelp" class="form-text small col-sm-9">
                Materi berupa link video Youtube
                </div>
              </div>
              <div class="row mb-3">
                <label for="formFileSummary" class="col-sm-3 form-label">Summarization</label>
                <div class="input-group col-sm-9">
                  <input class="form-control" type="file" accept=".doc,.docx,.pdf" id="formFileSummary" aria-describedby="FileSummaryHelp" name="materi_summarization">
                </div>
                <div class="col-sm-3"></div>
                <div id="FileSummaryHelp" class="form-text small col-sm-9">
                Materi berupa modul dengan format PDF
                </div>  
              </div>
              <div class="row mt-3">
                <label for="formFileMindMap" class="col-sm-3 form-label">Mind Mapping</label>
                <div class="input-group col-sm-9">
                  <input type="file" class="form-control" accept=".ppt,.pptx,.pdf" id="formFileMindMap" name="materi_mind_mapping">
                </div>
                <div class="col-sm-3"></div>
                <div id="FileMindMapHelp" class="form-text small col-sm-9">
                Materi berupa slide dengan format PPT, PPTX, atau PDF
                </div>  
              </div>
            </form>
                <div class="d-grid text-right">
                  <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                  <button class="btn btn-primary" style="font-weight: bold;width: 100px;" onclick='uploadMateri();' name="id" value="<?php $id ?>">SIMPAN</button> 
                </div>
              </div>
                </form>
            </div>
          </div>
          <div class="row">

          <?php 
            foreach($materials as $material) {
              echo '
              <div class="col-md-4">
              <div class="card text-center mt-4 mb-5">
                <img src="assets/uploads/gambar/'. $material->gambar .'" class="card-img-top mt-3" alt="'. $material->judul .'" width="150px" height="150px">
                <div class="card-body" id="card-body-nm">
                  <h5 class="card-text-nt">'. $material->judul .'</h5>
                  <p class="card-text">'. $material->deskripsi .'</p>
                  <button type="button" class="btn btn-danger mr-2" data-toggle="modal" data-target="#ModalHapusMateri" style="font-weight: bold;width: 100px;">HAPUS</button>
                  <div class="modal fade" id="ModalHapusMateri" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalHapusMateriLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
                  <div class="modal-content">
                    <div class="modal-body text-center">
                      <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                      <h6 class="modal-title mt-2" id="ModalHapusMateriLabel">Apakah kamu yakin akan menghapus materi ini?</h6>
                      <div class="d-grid mt-3">
                        <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">TIDAK</button>
                        <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">YA</a> 
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                  <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">EDIT</a> 
                </div>
              </div>
            </div>
              ';
            }
          ?>
          </div>
        </div>
      </div>
    </div>
    </div>

    <!-- Optional JavaScript-->

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
  </body>
</html>

<?php
 echo $OUTPUT->footer();
?>


