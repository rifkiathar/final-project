<?php

require_once(__DIR__ . '/../../../config.php');

global $DB;

$folderUploadEval = "./assets/uploads/evaluasinonmandiri";

$id = $_GET['id'];
$task_id = $_GET['taskid'];
date_default_timezone_set('Asia/Jakarta');
echo $id;
echo $taskid;
$date = date("YmdHis", time());
$filename = $USER->username . "_" . strval($USER->id) . "_" . strval($task_id);

# periksa apakah folder tersedia
if (!is_dir($folderUploadEval)) {
# jika tidak maka folder harus dibuat terlebih dahulu
mkdir($folderUploadEval, 0777, $rekursif = true);
}

# simpan masing-masing file ke dalam array 
# dan casting menjadi objek
$fileUnggahan = (object) @$_FILES['unggahan'];

// if ($fileFoto->size > 1000 * 2000) {
//   die("File tidak boleh lebih dari 1MB");
// }

// if ($fileGambar->type !== 'image/jpeg') {
//   die("File ktp harus jpeg!");
// }

# mulai upload file

$uploadUnggahanSukses = move_uploaded_file(
    $fileUnggahan->tmp_name, "{$folderUploadEval}/{$filename}"
);

$recordtoinsert = new stdClass();

$recordtoinsert->class_id = 1;
$recordtoinsert->task_id = 1;
// $recordtoinsert->class_id = $id;
// $recordtoinsert->task_id = $task_id;
$recordtoinsert->filename = $filename;
$recordtoinsert->date = $date;

$DB->insert_record('local_srl_eval_submit', $recordtoinsert);

// echo json_encode($recordtoinsert);

// redirect($CFG->wwwroot . '/local/srl/srlclass/evaluasinonmandiri.php?id=' . $id);
redirect($CFG->wwwroot . '/local/srl/srlclass/evaluasinonmandiri.php?id=1');


// if ($uploadFotoSukses) {
//   $link = "{$folderUpload}/{$fileFoto->name}";
//   echo "Sukses Upload Foto: <a href='{$link}'>{$fileFoto->name}</a>";
//   echo "<br>";
// }

// if ($uploadKtpSukses) {
//   $link = "{$folderUpload}/{$fileKtp->name}";
//   echo "Sukses Upload KTP: <a href='{$link}'>{$fileKtp->name}</a>";
//   echo "<br>";
// }

