<?php

require_once(__DIR__ . '/../../../config.php');
$PAGE->set_url(new moodle_url('/local/srl/srlclass/tesapk.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Tes APK');

global $DB;
global $SESSION;

$id = $_GET['classid'];
$materials_id = $_GET['materialsid'];
$apkid = $materials_id - 1;
//  $id = $SESSION->class_id;

$getEval = "SELECT * FROM {local_self_eval_quest} p WHERE p.class_id = :classid AND p.materials_id= :materialsid";
$paramEval = array('classid' => $id, 'materialsid' => $apkid);
$questions = $DB->get_records_sql($getEval, $paramEval);
$number = 1;


echo $OUTPUT->header();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <title>Activating Prior Knowledge: Tree</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item pl-0"><a href="#">Mata Kuliah</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes APK</li>
            </ol>
          </nav>
        </div>
      </div>
    <div class="row">
        <div class="card w-100 mt-4">
          <div class="card-body"  id="card-body-nm">
            <h5 class="card-text-nt text-center mt-4 mb-5">Tes Activating Prior Knowledge: Tree</h5>
            <iframe name="selfeval" style="display:none;"></iframe>
            <form class="container-fluid px-5" method="post" target="selfeval"> 
            <?php 
              foreach($questions as $question) {
                
                echo '
                <table class="table table-borderless table-sm">
                  <thead class="table-light" style="background-color: whitesmoke;">
                    <tr>
                      <th scope="col" style="width: 4%;">' . $number .'.</th>
                      <th scope="col" style="width: 96%;">'. $question->question .'</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'a" value="1">
                        <label class="form-check-label" for="' . $number .'a">A.' . $question->quest_a .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'b" value="2">
                        <label class="form-check-label" for="' . $number .'b">B.' . $question->quest_b .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'c" value="3">
                        <label class="form-check-label" for="' . $number .'c">C.' . $question->quest_c .'</label>
                      </div></td>
                    </tr>
                    <tr>
                      <th scope="row"></th>
                      <td><div class="form-check">
                        <input class="form-check-input" type="radio" name="'. $question->self_eval_quest_id .'" id="' . $number .'c" value="4">
                        <label class="form-check-label" for="' . $number .'d">D.' . $question->quest_d .'</label>
                      </div></td>
                    </tr>
                  </tbody>
                </table>
                ';
                $number = $number + 1;
              }
             ?>
            <div class="mb-3 mt-4">
            <button type="submit" class="btn btn-primary float-right mt-1" data-toggle="modal" data-target="#ModalReviewAPK" style="font-weight: bold;">SELESAI</button>
            <div class="modal fade" id="ModalReviewAPK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalReviewAPKLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
              <div class="modal-content">
                  <div class="modal-body text-center">
                    <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                    <div class="d-inline-block" style="color: #EFA30A;"><i class="fas fa-star"></i></div>
                    <h6 class="d-inline-block modal-title mt-2" id="ModalReviewAPKLabel" style="color: #EFA30A;"></h6>
                    <div class="small mt-2">Selamat, kamu telah menyelesaikan tes Activating Prior Knowledge. Selanjutnya kamu dapat melanjutkan mempelajari materi yang tadi kamu pilih, atau kembali ke halaman kelas.</div>
                    <div class="d-grid mt-3">
                      <a href="index.php?id=<?php echo $id?>" class="btn btn-secondary mr-1" style="font-weight: bold;width: 100px;">KEMBALI</a>
                      <a href="rekomendasi.php?id=<?php echo $id?>&materials=<?php echo $materials_id?>" class="btn btn-primary" style="font-weight: bold;width: 100px;">LANJUT</a> 
                    </div>
                  </div>
              </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
    </div>

    <!-- Optional JavaScript-->

    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php
echo $OUTPUT->footer();
?>
