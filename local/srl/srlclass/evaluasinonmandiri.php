<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/evaluasinonmandiri.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 echo $OUTPUT->header();

 global $DB;

 $id = $_GET['id'];

 $getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
 $paramClass = array('userid' => $id);
 $class = $DB->get_record_sql($getClass, $paramClass);

 $getTodo = "SELECT * FROM {local_srl_class_eval_task} p WHERE p.class_id = :classid";
 $paramTodo = array('classid' => $id);
 $todo = $DB->get_records_sql($getTodo, $paramTodo);

?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Evaluasi Non Mandiri: Algoritme dan Struktur Data</title>
   </head>
   <body>
     <div class="container">
       <div class="row">
         <div class="card mt-3 w-100" style="border-width: 0px !important;">
           <nav aria-label="breadcrumb">
             <ol class="breadcrumb breadcrumb-arrow p-0">
               <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
               <li class="breadcrumb-item pl-0"><a href="index.php?id=<?php echo $id?>">Mata Kuliah</a></li>
               <li aria-current="page" class="breadcrumb-item active">Evaluasi Non Mandiri</li>
             </ol>
           </nav>
         </div>
       </div>
     <div class="row">
         <div class="card-course w-100 mt-4">
             <div class="row no-gutters">
             <div class="col-md-2>">
                 <img src="img/ASD.png" class="card-img-center ml-4 mt-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
             </div>
             <div class="col-md-10">
              <div class="card-body" id="card-body-nm">
                  <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                  <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                  <p class="card-text-course m-1"><?php echo $class->year ?></p>
                  <p class="card-text-course m-1"><?php echo $class->code ?></p>
                </div>
             </div>
             </div>
           </div>
     </div>
     <div class="row">
       <div class="card w-75 mt-4">
         <ul class="nav nav-justified">
           <li class="nav-item">
             <a class="nav-link" href="index.php?id=<?php echo $id ?>">Materi Kuliah</a>
           </li>
           <li class="nav-item">
             <a class="nav-link active" href="#">Evaluasi Non Mandiri</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="forumdiskusi.php?id=<?php echo $id ?>">Forum Diskusi</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="hasilbelajar.php?id=<?php echo $id ?>">Hasil Belajar</a>
           </li>
         </ul>
       <div class="container px-5">
         <?php
          foreach($todo as $singleTodo) {
            $getSubmission = "SELECT * FROM {local_srl_eval_submit} p WHERE p.class_id = :classid AND p.task_id = :taskid";
            $paramSubmission = array('classid' => $id, 'taskid' => $singleTodo->eval_task_id);
            $submission = $DB->get_records_sql($getSubmission, $paramSubmission);

            echo '
            <div class="row">
                <div class="col-md-12">
                  <form class="card-body" id="card-body-nm" action="unggaheval.php?id='. $id .'&taskid='.$singleTodo->eval_task_id.'">
                    <h5 class="card-text-nt m-1 mb-3">'. $singleTodo->title .'</h5>
                    <p class="card-text m-1">'. $singleTodo->deskripsi .'</p>
                    <p class="card-text m-1 mt-3" style="font-weight: bold;">Batas Waktu: '. $singleTodo->due_date .'</p>';
            if(count($submission) != 0) {
              echo '<p class="card-text m-1 mt-3" style="font-weight: bold;">Terakhir diunggah: '. reset($submission)->date .'</p>';
            }
            echo '
                    <div class="custom-file mt-2">
                        <input type="file" class="custom-file-input" id="FileSubmitEvalNM1" name="unggahan"/>
                        <label class="custom-file-label" for="FileSubmitEvalNM1" style="border-color: #28527A;">Unggah pekerjaan</label>
                    </div>
                    <button type="submit" class="btn btn-primary float-right mt-3 mb-3" style="font-weight: bold;" id="unggahEval">KIRIM</button>
                  </form>
                </div>
              </div>
            ';
          }
         ?>
           <!-- <div class="card w-100 mt-5 mb-5">
               <div class="row">
                 <div class="col-md-12">
                   <div class="card-body" id="card-body-nm">
                     <h5 class="card-text-nt m-1 mb-3">Tugas Kelompok 1</h5>
                     <p class="card-text m-1">Buatlah program Stack dengan bahasa C. Format berisi source code dan screenshot program.</p>
                     <p class="card-text m-1 mt-3" style="font-weight: bold;">Due date: 30 April 2021, 23:59</p>
                     <p class="card-text m-1" style="font-weight: bold;">Last modified: -</p>
                     <div class="m-1 mt-3">  
                       <input class="form-control" type="file" id="formFile">
                     </div>
                       <button type="submit" class="btn btn-primary float-right m-1 mt-3 mb-3" disabled style="font-weight: bold;">KIRIM</button>
                   </div>
                 </div>
               </div>
               </div> -->
           </div>
         </div>
       <div class="col-md-3">
         <div class="card mt-4 ml-3">
           <div class="card-body" id="card-body-nm">
             <h5 class="card-title m-2">To-Do List</h5>
             <div class="list-group mt-4 m-1">
              <?php 
                  foreach($todo as $singleTodo) {
                    echo '
                    <a href="#" class="list-group-item list-group-item-action" id="list-group-item-course">
                      <p class="mb-1">'. $singleTodo->title .'</p>
                      <small>'. $singleTodo->due_date .'</small>
                    </a>
                    ';
                  }
                ?>
             </div>
           </div>
           </div>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();
 ?>