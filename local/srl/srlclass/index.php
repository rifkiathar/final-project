<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/srlclass/index.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 global $DB;
 global $SESSION;

 $id = $_GET['id'];
//  $id = $SESSION->class_id;

 $getLo = "SELECT * FROM {local_class_lo} p WHERE p.class_id = :userid";
 $paramLo = array('userid' => $id);
 $lo = $DB->get_records_sql($getLo, $paramLo);

 $getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
 $paramClass = array('userid' => $id);
 $class = $DB->get_record_sql($getClass, $paramLo);

 $getMaterials = "SELECT * FROM {local_srl_class_materials} p WHERE p.class_id = :classid";
 $paramMaterials = array('classid' => $id);
 $materials = $DB->get_records_sql($getMaterials, $paramMaterials);

 $getTodo = "SELECT * FROM {local_srl_class_eval_task} p WHERE p.class_id = :classid";
 $paramTodo = array('classid' => $id);
 $todo = $DB->get_records_sql($getTodo, $paramTodo);

 $getSelfResult = "SELECT * FROM {local_srl_self_eval} p WHERE p.class_id = :classid AND user_id = :userid";
 $paramSelfResult = array('classid' => $id, 'userid' => $USER->id);
 $selfResult = $DB->get_records_sql($getSelfResult, $paramSelfResult);


 echo $OUTPUT->header();

 ?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <title>Course: Algoritme dan Struktur Data</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="../../../my">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Mata Kuliah</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="row">
        <div class="card-course w-100 mt-4">
            <div class="row no-gutters">
            <div class="col-md-2>">
                <img src="img/ASD.png" class="card-img-center mt-4 ml-4" alt="Algoritme dan Struktur Data" width="100px" height="100px">
            </div>
            <div class="col-md-10">
              <div class="card-body" id="card-body-nm">
                <h5 class="card-text-nt m-1"><?php echo $class->class_name ?></h5>
                <p class="card-text-course m-1"><?php echo $class->lecturer ?></p>
                <p class="card-text-course m-1"><?php echo $class->year ?></p>
                <p class="card-text-course m-1"><?php echo $class->code ?></p>
              </div>
            </div>
            </div>
          </div>
      </div>
    <div class="row">
      <div class="card w-75 mt-4">
        <ul class="nav nav-justified">
           <li class="nav-item">
             <a class="nav-link active" href="#">Materi Kuliah</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="evaluasinonmandiri.php?id=<?php echo $id ?>">Evaluasi Non Mandiri</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="forumdiskusi.php?id=<?php echo $id ?>">Forum Diskusi</a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="hasilbelajar.php?id=<?php echo $id ?>">Hasil Belajar</a>
           </li>
         </ul>
      <div class="container px-5">
            <table class="table table-striped mt-5">
              <thead class="table-dark">
                <tr>
                  <th scope="col"></th>
                  <th scope="col">Learning Outcomes</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach($lo as $singleLO) {
                ?>
                <tr>
                  <td scope="row"><?php echo $singleLO->lo_id ?></td>
                  <td><?php echo $singleLO->lo ?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
            </table>
        <h5 class="card-title mt-3">Materi Kuliah</h5>
          <div class="row">
            <?php 
              foreach($materials as $material) {
                echo '
                <div class="col-md-4">
                  <div class="card text-center mt-4 mb-5">
                    <img src="assets/uploads/gambar/'. $material->gambar .'" class="card-img-top mt-3" alt="Tree" width="150px" height="150px">
                    <div class="card-body" id="card-body-nm">
                      <h5 class="card-text-nt">'. $material->judul .'</h5>
                      <p class="card-text">'. $material->deskripsi .'</p>
                       ';
                if($material != reset($materials) && count($selfResult) < 1) {
                  
                  echo '<a href="rekomendasi.php?id='. $id .'&materials='. $material->materials_id .'" class="btn btn-primary btn-block" style="font-weight: bold;"  data-toggle="modal" data-target="#ModalAPK" onclick="getMaterialsId()">PILIH</a>
                  <div class="modal fade" id="ModalAPK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalAPKLabel" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
                  <div class="modal-content">
                      <div class="modal-body text-center">
                        <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                        <h6 class="modal-title mt-2" id="ModalAPKLabel" style="color: #EFA30A;"">Activating Prior Knowledge</h6>
                        <div class="small mt-2">Kamu belum menyelesaikan materi sebelumnya. Yuk, ikuti tes Activating Prior Knowledge terlebih dahulu untuk dapat memilih materi ini.</div>
                        <div class="d-grid mt-3">
                          <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                          <a href="tesapk.php?classid='. $id .'&materialsid='. $material->materials_id .'" class="btn btn-primary" style="font-weight: bold;width: 100px;">MULAI</a> 
                        </div>
                      </div>
                  </div>
                  </div>
                </div> ';
                } else {
                  echo '<a href="rekomendasi.php?id='. $id .'&materials='. $material->materials_id .'" class="btn btn-primary btn-block" style="font-weight: bold;" onclick="getMaterialsId()">PILIH</a>';
                }
                      
                echo '
                    </div>
                  </div>
                </div>
                ';
              }
            ?>
            <!-- <div class="col-md-4">
              <div class="card text-center mt-4 mb-5">
                <img src="img/ic_tree.svg" class="card-img-top mt-3" alt="Tree" width="150px" height="150px">
                <div class="card-body" id="card-body-nm">
                  <h5 class="card-text-nt">Tree</h5>
                  <p class="card-text">Struktur data yang terdiri dari akar dan subpohon-subpohon dalam susunan hierarki.</p>
                  <a href="rekomendasi.php?id=<?php echo $id ?>" class="btn btn-primary btn-block" style="font-weight: bold;">PILIH</a> 
                </div>
              </div>
            </div> -->
            <!-- <div class="col-md-4">
              <div class="card text-center mt-4">
                <img src="img/ic_sorting.svg" class="card-img-top mt-3" alt="Sorting" width="150px" height="150px">
                <div class="card-body" id="card-body-nm">
                  <h5 class="card-text-nt">Sorting</h5>
                  <p class="card-text">Metode yang digunakan untuk mengatur sekumpulan objek menurut susunan tertentu.</p>
                  <a href="#" class="btn btn-primary btn-block" style="font-weight: bold;" data-toggle="modal" data-target="#ModalAPK">PILIH</a>
                  <div class="modal fade" id="ModalAPK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalAPKLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
                    <div class="modal-content">
                        <div class="modal-body text-center">
                          <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                          <h6 class="modal-title mt-2" id="ModalAPKLabel" style="color: #EFA30A;"">Activating Prior Knowledge</h6>
                          <div class="small mt-2">Kamu belum menyelesaikan materi sebelumnya. Yuk, ikuti tes Activating Prior Knowledge terlebih dahulu untuk dapat memilih materi ini.</div>
                          <div class="d-grid mt-3">
                            <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                            <a href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">MULAI</a> 
                          </div>
                        </div>
                    </div>
                    </div>
                  </div> 
                </div>
              </div>
            </div> -->
            <!-- <div class="col-md-4">
              <div class="card text-center mt-4">
                <img src="img/ic_materi search.svg" class="card-img-top mt-3" alt="Search" width="150px" height="150px">
                <div class="card-body" id="card-body-nm">
                  <h5 class="card-text-nt">Search</h5>
                  <p class="card-text">Metode yang digunakan untuk melakukan proses pencarian dari sekumpulan objek.</p>
                  <a href="#" class="btn btn-primary disabled btn-block" tabindex="-1" style="font-weight: bold;" aria-disabled="true">PILIH</a> 
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="card mt-4 ml-3">
          <div class="card-body" id="card-body-nm">
            <h5 class="card-title m-2">To-Do List</h5>
            <div class="list-group mt-4 m-1">
              <?php 
                foreach($todo as $singleTodo) {
                  echo '
                  <a href="#" class="list-group-item list-group-item-action" id="list-group-item-course">
                    <p class="mb-1">'. $singleTodo->title .'</p>
                    <small>'. $singleTodo->due_date .'</small>
                  </a>
                  ';
                }
              ?>
            </div>
          </div>
          </div>
      </div>
    </div>
    </div>

    <!-- Optional JavaScript-->

    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <script>
      function getMaterialsId() {
        <?php 
          $SESSION->materials_id = $material->materials_id;
          ?>
      }
    </script>
    
  </body>
</html>

<?php
 echo $OUTPUT->footer();
?>
