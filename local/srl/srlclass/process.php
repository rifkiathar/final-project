<?php

require_once(__DIR__ . '/../../../config.php');

global $DB;

if(isset($_GET['id'])) {
  $folderUploadGambar = "./assets/uploads/gambar";
  $folderUploadSum = "./assets/uploads/summarization";
  $folderUploadMindMap = "./assets/uploads/mind_mapping";

  $id = $_GET['id'];
  date_default_timezone_set('Asia/Jakarta');
  echo $id;
  $judul = $_POST['judul'];
  $desc = $_POST['desc'];
  $link = $_POST['link'];
  $date = date("YmdHis", time());

  # periksa apakah folder tersedia
  if (!is_dir($folderUploadGambar)) {
    # jika tidak maka folder harus dibuat terlebih dahulu
    mkdir($folderUploadGambar, 0777, $rekursif = true);
  }

  # periksa apakah folder tersedia
  if (!is_dir($folderUploadSum)) {
    # jika tidak maka folder harus dibuat terlebih dahulu
    mkdir($folderUploadSum, 0777, $rekursif = true);
  }

  # periksa apakah folder tersedia
  if (!is_dir($folderUploadMindMap)) {
    # jika tidak maka folder harus dibuat terlebih dahulu
    mkdir($folderUploadMindMap, 0777, $rekursif = true);
  }

  # simpan masing-masing file ke dalam array 
  # dan casting menjadi objek
  $fileGambar = (object) @$_FILES['gambar_materi'];
  $fileMateriSummarization = (object) @$_FILES['materi_summarization'];
  $fileMateriMindMapping = (object) @$_FILES['materi_mind_mapping'];

  // if ($fileFoto->size > 1000 * 2000) {
  //   die("File tidak boleh lebih dari 1MB");
  // }

  // if ($fileGambar->type !== 'image/jpeg') {
  //   die("File ktp harus jpeg!");
  // }

  # mulai upload file

  $uploadGambarSukses = move_uploaded_file(
    $fileGambar->tmp_name, "{$folderUploadGambar}/{$fileGambar->name}"
  );

  $uploadMateriSumSukses = move_uploaded_file(
    $fileMateriSummarization->tmp_name, "{$folderUploadSum}/{$fileMateriSummarization->name}"
  );

  $uploadMateriMindMapSukses = move_uploaded_file(
    $fileMateriMindMapping->tmp_name, "{$folderUploadMindMap}/{$fileMateriMindMapping->name}"
  );

  $recordtoinsert = new stdClass();
  $recordtoinsert->class_id = $id;
  $recordtoinsert->judul = $judul;
  $recordtoinsert->deskripsi = $desc;
  $recordtoinsert->gambar = $fileGambar->name;
  $recordtoinsert->control_video = $link;
  $recordtoinsert->summarization = $fileMateriSummarization->name;
  $recordtoinsert->mind_mapping = $fileMateriMindMapping->name;
  $recordtoinsert->date = $date;

  $DB->insert_record('local_srl_class_materials', $recordtoinsert);

  redirect($CFG->wwwroot . '/local/srl/srlclass/admin.php?id=' . $id);


  // if ($uploadFotoSukses) {
  //   $link = "{$folderUpload}/{$fileFoto->name}";
  //   echo "Sukses Upload Foto: <a href='{$link}'>{$fileFoto->name}</a>";
  //   echo "<br>";
  // }

  // if ($uploadKtpSukses) {
  //   $link = "{$folderUpload}/{$fileKtp->name}";
  //   echo "Sukses Upload KTP: <a href='{$link}'>{$fileKtp->name}</a>";
  //   echo "<br>";
  // }
}

