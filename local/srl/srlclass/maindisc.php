<?php

require_once(__DIR__ . '/../../../config.php');

global $DB;

$id = $_GET['id'];
$maindisc = $_POST['maindisc'];

date_default_timezone_set('Asia/Jakarta');
$date = date("YmdHis", time());

$recordtoinsert1 = new stdClass();

$recordtoinsert1->class_id = $id;
$recordtoinsert1->name = $USER->username;
$recordtoinsert1->disc = $maindisc;
$recordtoinsert1->date = $date;

$DB->insert_record('local_main_disc', $recordtoinsert1);

redirect($CFG->wwwroot . '/local/srl/srlclass/forumdiskusi.php?id=' . $id);