<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/vak/vak3.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 echo $OUTPUT->header();

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <form class="container-fluid px-5" method="post">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">11.</th>
                   <th scope="col" style="width: 96%;"> Jika saya memilih makanan pada daftar menu, saya cenderung untuk:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no11" id="11a" value="1">
                     <label class="form-check-label" for="11a">A. membayangkan makanan akan seperti apa.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no11" id="11b" value="2">
                     <label class="form-check-label" for="11b">B. memikirkannya sendiri atau membicarakannya dengan pasangan saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no11" id="11c" value="3">
                     <label class="form-check-label" for="11c">C. membayangkan makanan akan terasa seperti apa.
                     </label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">12.</th>
                   <th scope="col" style="width: 96%;">Ketika saya mendengarkan pertunjukan sebuah band, saya tidak bisa:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no12" id="12a" value="1">
                     <label class="form-check-label" for="12a">A. melihat anggota band dan orang lain di antara para penonton.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no12" id="12b" value="2">
                     <label class="form-check-label" for="12b">B. mendengarkan lirik dan nada.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no12" id="12c" value="3">
                     <label class="form-check-label" for="12c">C. terbawa dalam suasana dan musik.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">13.</th>
                   <th scope="col" style="width: 96%;">Ketika saya berkonsentrasi, saya paling sering:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no13" id="13a" value="1">
                     <label class="form-check-label" for="13a">A. fokus pada kata-kata atau gambar-gambar di depan saya</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no13" id="13b" value="2">
                     <label class="form-check-label" for="13b">B. membahas masalah dan memikirkan solusi yang mungkin dapat dilakukan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no13" id="13c" value="3">
                     <label class="form-check-label" for="13c">C. banyak melihat hal di sekitar, mencatat yang diperlukan.
                     </label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">14.</th>
                   <th scope="col" style="width: 96%;">Saya memilih peralatan rumah tangga karena saya suka:
                   </th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no14" id="14a" value="1">
                     <label class="form-check-label" for="14a">A. warnanya dan bagaimana penampilannya.
                     </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no14" id="14b" value="2">
                     <label class="form-check-label" for="14b">B. paparan dari pramuniaga.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no14" id="14c" value="3">
                     <label class="form-check-label" for="14c">C. tekstur peralatan tersebut dan bagaimana rasanya menyentuhnya.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">15.</th>
                   <th scope="col" style="width: 96%;">Memori pertama saya terbentuk ketika ...</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no15" id="15a" value="1">
                     <label class="form-check-label" for="15a">A. melihat sesuatu.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no15" id="15b" value="2">
                     <label class="form-check-label" for="15b">B. sedang membicarakannya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no15" id="15c" value="3">
                     <label class="form-check-label" for="15c">C. melakukan sesuatu.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>1</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>2</button></li>
                 <li class="page-item"><button class="page-link active" href="#" disabled>3</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit">4</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>5</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button type="submit" class="btn btn-primary float-right mt-1" disabled style="font-weight: bold;">SELESAI</button>
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

      function getAllAnswer() {

        <?php 
          if(!empty($_POST ['no11']) && !empty($_POST ['no12']) && !empty($_POST ['no13']) && !empty($_POST ['no14']) && !empty($_POST ['no15'])) {
            $record1 = new stdClass();
            $record1->user_id = $USER->id;
            $record1->vak_quest_id = "11";
            $record1->vak_user_answer = $_POST['no11'];

            $DB->insert_record('local_vak_answer', $record1);

            $record2 = new stdClass();
            $record2->user_id = $USER->id;
            $record2->vak_quest_id = "12";
            $record2->vak_user_answer = $_POST['no12'];

            $DB->insert_record('local_vak_answer', $record2);

            $record3 = new stdClass();
            $record3->user_id = $USER->id;
            $record3->vak_quest_id = "13";
            $record3->vak_user_answer = $_POST['no13'];

            $DB->insert_record('local_vak_answer', $record3);

            $record4 = new stdClass();
            $record4->user_id = $USER->id;
            $record4->vak_quest_id = "14";
            $record4->vak_user_answer = $_POST['no14'];

            $DB->insert_record('local_vak_answer', $record4);

            $record5 = new stdClass();
            $record5->user_id = $USER->id;
            $record5->vak_quest_id = "15";
            $record5->vak_user_answer = $_POST['no15'];

            $DB->insert_record('local_vak_answer', $record5);

            redirect($CFG->wwwroot . '/local/srl/vak/vak4.php');
          }
            
           
        ?>
       
      }
    </script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();

 ?>