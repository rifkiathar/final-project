<?php

require_once(__DIR__ . '/../../../config.php');
 
 global $DB;

 

 $PAGE->set_url(new moodle_url('/local/srl/vak/vak1.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

//  $data = $DB->get_record_sql('SELECT * FROM {local_vak_answer} WHERE user_id = $USER->id');
//  $user = $DB->get_record('local_vak_answer', ['user_id' => $USER->id]);
//  echo serialize($user);

$DB->delete_records('local_vak_answer',array('user_id' => $USER->id));

// $getInfo = "SELECT * FROM {local_user} p WHERE p.user_moodle_id = :userid";
//   $paramInfo = array('userid' => $USER->id);
//   $resultInfo = $DB->get_records_sql($getInfo, $paramInfo);


 echo $OUTPUT->header();

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <form class="container-fluid px-5" method="post">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">1.</th>
                   <th scope="col" style="width: 96%;">Ketika saya mengoperasikan peralatan baru, pada umumnya saya:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no1" id="1a" value="1">
                     <label class="form-check-label" for="1a">A. membaca instruksinya lebih dulu.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no1" id="1b" value="2">
                     <label class="form-check-label" for="1b">B.  mendengarkan penjelasan dari seseorang yang pernah menggunakan peralatan tersebut. </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no1" id="1c" value="3">
                     <label class="form-check-label" for="1c">C. menggunakan peralatan tersebut, saya akan mampu mengetahuinya selagi menggunakannya.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">2.</th>
                   <th scope="col" style="width: 96%;">Ketika saya perlu petunjuk untuk bepergian, saya biasanya: </th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no2" id="2a" value="1">
                     <label class="form-check-label" for="2a">A. melihat peta. </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no2" id="2b" value="2">
                     <label class="form-check-label" for="2b">B. bertanya denah/arah ke orang lain </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no2" id="2c" value="3">
                     <label class="form-check-label" for="2c">C. mengikuti naluri dan mungkin menggunakan kompas.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">3.</th>
                   <th scope="col" style="width: 96%;">Ketika saya memasak masakan baru, saya biasanya melakukan:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no3" id="3a" value="1">
                     <label class="form-check-label" for="3a">A. mengikuti petunjuk resep tertulis.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no3" id="3b" value="2">
                     <label class="form-check-label" for="3b">B. menelepon seorang teman untuk mendapatkan penjelasan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no3" id="3c" value="3">
                     <label class="form-check-label" for="3c">C. mengikuti naluri, mencoba seperti biasanya saya memasak.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">4.</th>
                   <th scope="col" style="width: 96%;"> Jika saya mengajar seseorang tentang sesuatu yang baru, saya cenderung untuk:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no4" id="4a" value="1">
                     <label class="form-check-label" for="4a">A.  menulis instruksi bagi mereka.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no4" id="4b" value="2">
                     <label class="form-check-label" for="4b">B. memberi penjelasan secara lisan kepada mereka.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no4" id="4c" value="3">
                     <label class="form-check-label" for="4c">C.  mendemonstrasikan terlebih dahulu dan kemudian membiarkan mereka berlanjut.
                     </label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">5.</th>
                   <th scope="col" style="width: 96%;">Saya cenderung mengatakan:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no5" id="5a" value="1">
                     <label class="form-check-label" for="5a">A. lihat bagaimana saya melakukannya</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no5" id="5b" value="2">
                     <label class="form-check-label" for="5b">B. dengarkan saya menjelaskan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no5" id="5c" value="3">
                     <label class="form-check-label" for="5c">C. lakukanlah sendiri.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link active" href="#" disabled>1</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit">2</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>3</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>4</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>5</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button type="submit" class="btn btn-primary float-right mt-1" disabled style="font-weight: bold;"'>SELESAI</button>
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

      function getAllAnswer() {


        var jawaban = $("input[name=no1]:checked").val();

        <?php 
            if(!empty($_POST ['no1']) && !empty($_POST ['no2']) && !empty($_POST ['no3']) && !empty($_POST ['no4']) && !empty($_POST ['no5'])) {
              $record1 = new stdClass();
              $record1->user_id = $USER->id;
              $record1->vak_quest_id = "1";
              $record1->vak_user_answer = $_POST['no1'];

              $DB->insert_record('local_vak_answer', $record1);

              $record2 = new stdClass();
              $record2->user_id = $USER->id;
              $record2->vak_quest_id = "2";
              $record2->vak_user_answer = $_POST['no2'];

              $DB->insert_record('local_vak_answer', $record2);

              $record3 = new stdClass();
              $record3->user_id = $USER->id;
              $record3->vak_quest_id = "3";
              $record3->vak_user_answer = $_POST['no3'];

              $DB->insert_record('local_vak_answer', $record3);

              $record4 = new stdClass();
              $record4->user_id = $USER->id;
              $record4->vak_quest_id = "4";
              $record4->vak_user_answer = $_POST['no4'];

              $DB->insert_record('local_vak_answer', $record4);

              $record5 = new stdClass();
              $record5->user_id = $USER->id;
              $record5->vak_quest_id = "5";
              $record5->vak_user_answer = $_POST['no5'];

              $DB->insert_record('local_vak_answer', $record5);

              redirect($CFG->wwwroot . '/local/srl/vak/vak2.php');

            }
            
        ?>
       
      }
    </script>
   </body>
 </html>

 <?php
 echo $OUTPUT->footer();
 ?>