<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/vak/vak5.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 echo $OUTPUT->header();

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <form class="container-fluid px-5" method="post">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">21.</th>
                   <th scope="col" style="width: 96%;">Sebagian besar waktu luang, saya habiskan:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no21" id="21a" value="1">
                     <label class="form-check-label" for="21a">A. menonton televisi.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no21" id="21b" value="2">
                     <label class="form-check-label" for="21b">B. berbincang-bincang dengan teman-teman.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no21" id="21c" value="3">
                     <label class="form-check-label" for="21c">C. melakukan aktivitas fisik atau membuat sesuatu.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">22.</th>
                   <th scope="col" style="width: 96%;">Ketika pertama kali saya bertemu seseorang yang baru, saya biasanya:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no22" id="22a" value="1">
                     <label class="form-check-label" for="22a">A. mengatur pertemuan bersama.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no22" id="22b" value="2">
                     <label class="form-check-label" for="22b">B. berbicara dengan mereka melalui telepon.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no22" id="22c" value="3">
                     <label class="form-check-label" for="22c">C. mencoba melakukan sesuatu bersama-sama, misalnya suatu kegiatan atau makan bersama.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">23.</th>
                   <th scope="col" style="width: 96%;">Saya memperhatikan orang melalui:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no23" id="23a" value="1">
                     <label class="form-check-label" for="23a">A. tampilannya dan pakaiannya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no23" id="23b" value="2">
                     <label class="form-check-label" for="23b">B. suara dan cara berbicaranya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no23" id="23c" value="3">
                     <label class="form-check-label" for="23c">C. caranya berdiri dan bergerak.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">24.</th>
                   <th scope="col" style="width: 96%;">Jika saya marah, saya cenderung untuk:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no24" id="24a" value="1">
                     <label class="form-check-label" for="24a">A. terus mengingat dan mencari tahu hal yang membuat saya marah</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no24" id="24b" value="2">
                     <label class="form-check-label" for="24b">B. menyampaikan ke orang-orang sekitar tentang perasaan saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no24" id="24c" value="3">
                     <label class="form-check-label" for="24c">C. membanting pintu atau menunjukkan kemarahan saya dengan cara lainnya.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">25.</th>
                   <th scope="col" style="width: 96%;">Saya merasa lebih mudah untuk mengingat:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no25" id="25a" value="1">
                     <label class="form-check-label" for="25a">A. wajah.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no25" id="25b" value="2">
                     <label class="form-check-label" for="25b">B. nama.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no25" id="25c" value="3">
                     <label class="form-check-label" for="25c">C. hal-hal yang telah saya lakukan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>1</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>2</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>3</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>4</button></li>
                 <li class="page-item"><button class="page-link active" href="#" disabled>5</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit">6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button type="submit" class="btn btn-primary float-right mt-1" disabled style="font-weight: bold;">SELESAI</button>
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

      function getAllAnswer() {

        <?php 
          if(!empty($_POST ['no21']) && !empty($_POST ['no22']) && !empty($_POST ['no23']) && !empty($_POST ['no24']) && !empty($_POST ['no25'])) {
            $record1 = new stdClass();
            $record1->user_id = $USER->id;
            $record1->vak_quest_id = "21";
            $record1->vak_user_answer = $_POST['no21'];

            $DB->insert_record('local_vak_answer', $record1);

            $record2 = new stdClass();
            $record2->user_id = $USER->id;
            $record2->vak_quest_id = "22";
            $record2->vak_user_answer = $_POST['no22'];

            $DB->insert_record('local_vak_answer', $record2);

            $record3 = new stdClass();
            $record3->user_id = $USER->id;
            $record3->vak_quest_id = "23";
            $record3->vak_user_answer = $_POST['no23'];

            $DB->insert_record('local_vak_answer', $record3);

            $record4 = new stdClass();
            $record4->user_id = $USER->id;
            $record4->vak_quest_id = "24";
            $record4->vak_user_answer = $_POST['no24'];

            $DB->insert_record('local_vak_answer', $record4);

            $record5 = new stdClass();
            $record5->user_id = $USER->id;
            $record5->vak_quest_id = "25";
            $record5->vak_user_answer = $_POST['no25'];

            $DB->insert_record('local_vak_answer', $record5);

            redirect($CFG->wwwroot . '/local/srl/vak/vak6.php');
          }
            
           
        ?>
       
      }
    </script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();

 ?>