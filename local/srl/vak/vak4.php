<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/vak/vak4.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 echo $OUTPUT->header();

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <form class="container-fluid px-5" method="post">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">16.</th>
                   <th scope="col" style="width: 96%;">Ketika saya cemas, saya:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no16" id="16a" value="1">
                     <label class="form-check-label" for="16a">A. membayangkan skenario terburuk.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no16" id="16b" value="2">
                     <label class="form-check-label" for="16b">B. memikirkan apa yang paling mengkhawatirkan saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no16" id="16c" value="3">
                     <label class="form-check-label" for="16c">C. tidak dapat duduk tenang, mondar-mandir.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">17.</th>
                   <th scope="col" style="width: 96%;">Saya dapat mengingat orang lain, karena:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no17" id="17a" value="1">
                     <label class="form-check-label" for="17a">A. penampilan mereka.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no17" id="17b" value="2">
                     <label class="form-check-label" for="17b">B. apa yang mereka katakan kepada saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no17" id="17c" value="3">
                     <label class="form-check-label" for="17c">C. bagaimana mereka membuat saya mampu mengingatkan saya tentang mereka.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">18.</th>
                   <th scope="col" style="width: 96%;">Ketika saya harus merevisi untuk ujian, saya biasanya:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no18" id="18a" value="1">
                     <label class="form-check-label" for="18a">A. menulis banyak catatan revisi dan diagram.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no18" id="18b" value="2">
                     <label class="form-check-label" for="18b">B. menekuni catatan saya sendiri, atau membahasnya dengan orang lain.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no18" id="18c" value="3">
                     <label class="form-check-label" for="18c">C. membayangkan membuat kemajuan belajar atau menciptakan rumus/cara yang tepat.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">19.</th>
                   <th scope="col" style="width: 96%;">Jika saya menjelaskan kepada seseorang, saya cenderung:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no19" id="19a" value="1">
                     <label class="form-check-label" for="19a">A. menunjukkan kepada mereka apa yang saya maksud.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no19" id="19b" value="2">
                     <label class="form-check-label" for="19b">B. menjelaskan kepada mereka dengan cara yang memungkinkan sampai mereka paham.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no19" id="19c" value="3">
                     <label class="form-check-label" for="19c">C. memotivasi mereka untuk mencoba dan menyampaikan ide saya selagi mereka melakukan kegiatan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">20.</th>
                   <th scope="col" style="width: 96%;">Saya benar-benar suka:
                   </th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no20" id="20a" value="1">
                     <label class="form-check-label" for="20a">A. menonton film, fotografi, melihat seni atau mengamati orang-orang sekitar</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no20" id="20b" value="2">
                     <label class="form-check-label" for="20b">B. mendengarkan musik, radio atau bincang-bincang dengan teman-teman.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no20" id="20c" value="3">
                     <label class="form-check-label" for="20c">C. berperan serta dalam kegiatan olahraga, atau menikmati makanan yang disajikan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>1</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>2</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>3</button></li>
                 <li class="page-item"><button class="page-link active" href="#" disabled>4</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit">5</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button type="submit" class="btn btn-primary float-right mt-1" disabled style="font-weight: bold;">SELESAI</button>
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

      function getAllAnswer() {

        <?php 
          if(!empty($_POST ['no16']) && !empty($_POST ['no17']) && !empty($_POST ['no18']) && !empty($_POST ['no19']) && !empty($_POST ['no20'])) {
            $record1 = new stdClass();
            $record1->user_id = $USER->id;
            $record1->vak_quest_id = "16";
            $record1->vak_user_answer = $_POST['no16'];

            $DB->insert_record('local_vak_answer', $record1);

            $record2 = new stdClass();
            $record2->user_id = $USER->id;
            $record2->vak_quest_id = "17";
            $record2->vak_user_answer = $_POST['no17'];

            $DB->insert_record('local_vak_answer', $record2);

            $record3 = new stdClass();
            $record3->user_id = $USER->id;
            $record3->vak_quest_id = "18";
            $record3->vak_user_answer = $_POST['no18'];

            $DB->insert_record('local_vak_answer', $record3);

            $record4 = new stdClass();
            $record4->user_id = $USER->id;
            $record4->vak_quest_id = "19";
            $record4->vak_user_answer = $_POST['no19'];

            $DB->insert_record('local_vak_answer', $record4);

            $record5 = new stdClass();
            $record5->user_id = $USER->id;
            $record5->vak_quest_id = "20";
            $record5->vak_user_answer = $_POST['no20'];

            $DB->insert_record('local_vak_answer', $record5);

            redirect($CFG->wwwroot . '/local/srl/vak/vak5.php');
          }
            
           
        ?>
       
      }
    </script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();

 ?>