<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/vak/vak2.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

 echo $OUTPUT->header();

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <form class="container-fluid px-5" method="post">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">6.</th>
                   <th scope="col" style="width: 96%;">Selama waktu senggang, saya paling menikmati saat:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no6" id="6a" value="1">
                     <label class="form-check-label" for="6a">A. pergi ke museum dan galeri.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no6" id="6b" value="2">
                     <label class="form-check-label" for="6b">B. mendengarkan musik dan bincang-bincang dengan teman-teman saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no6" id="6c" value="3">
                     <label class="form-check-label" for="6c">C. bermain olahraga atau melakukan hal yang bisa saya lakukan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">7.</th>
                   <th scope="col" style="width: 96%;">Ketika saya pergi berbelanja pakaian, saya cenderung untuk:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no7" id="7a" value="1">
                     <label class="form-check-label" for="7a">A. membayangkan apakah pakaian tersebut sesuai bagi saya.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no7" id="7b" value="2">
                     <label class="form-check-label" for="7b">B. mendiskusikannya dengan karyawan toko.
                     </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no7" id="7c" value="3">
                     <label class="form-check-label" for="7c">C. mencoba pakaian dan melihat kesesuaiannya.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">8.</th>
                   <th scope="col" style="width: 96%;">Bila saya memilih liburan, saya biasanya:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no8" id="8a" value="1">
                     <label class="form-check-label" for="8a">A. membaca banyak brosur.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no8" id="8b" value="2">
                     <label class="form-check-label" for="8b">B. meminta rekomendasi dari teman-teman.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no8" id="8c" value="3">
                     <label class="form-check-label" for="8c">C. membayangkan akan seperti apa jika berada di sana</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">9.</th>
                   <th scope="col" style="width: 96%;">Jika saya membeli mobil baru, saya akan:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no9" id="9a" value="1">
                     <label class="form-check-label" for="9a">A. membaca ulasan di koran dan majalah.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no9" id="9b" value="2">
                     <label class="form-check-label" for="9b">B. membahas apa yang saya butuhkan dengan teman-teman.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no9" id="9c" value="3">
                     <label class="form-check-label" for="9c">C. melakukan test-drive banyak jenis.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">10.</th>
                   <th scope="col" style="width: 96%;">Ketika saya sedang belajar keterampilan baru, saya paling nyaman:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no10" id="10a" value="1">
                     <label class="form-check-label" for="10a">A. melihat apa yang pengajar lakukan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no10" id="10b" value="2">
                     <label class="form-check-label" for="10b">B. menanyakan ke pengajar tentang apa yang seharusnya saya lakukan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no10" id="10c" value="3">
                     <label class="form-check-label" for="10c">C. mencobanya dan menemukan sendiri ketika saya mempelajarinya.
                     </label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>1</button></li>
                 <li class="page-item"><button class="page-link active" href="#" disabled>2</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit">3</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>4</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>5</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button type="submit" class="btn btn-primary float-right mt-1" disabled style="font-weight: bold;">SELESAI</button>
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script type="text/javascript">

      function getAllAnswer() {

        <?php 
          if(!empty($_POST ['no6']) && !empty($_POST ['no7']) && !empty($_POST ['no8']) && !empty($_POST ['no9']) && !empty($_POST ['no10'])) {
            $record1 = new stdClass();
            $record1->user_id = $USER->id;
            $record1->vak_quest_id = "6";
            $record1->vak_user_answer = $_POST['no6'];

            $DB->insert_record('local_vak_answer', $record1);

            $record2 = new stdClass();
            $record2->user_id = $USER->id;
            $record2->vak_quest_id = "7";
            $record2->vak_user_answer = $_POST['no7'];

            $DB->insert_record('local_vak_answer', $record2);

            $record3 = new stdClass();
            $record3->user_id = $USER->id;
            $record3->vak_quest_id = "8";
            $record3->vak_user_answer = $_POST['no8'];

            $DB->insert_record('local_vak_answer', $record3);

            $record4 = new stdClass();
            $record4->user_id = $USER->id;
            $record4->vak_quest_id = "9";
            $record4->vak_user_answer = $_POST['no9'];

            $DB->insert_record('local_vak_answer', $record4);

            $record5 = new stdClass();
            $record5->user_id = $USER->id;
            $record5->vak_quest_id = "10";
            $record5->vak_user_answer = $_POST['no10'];

            $DB->insert_record('local_vak_answer', $record5);

            redirect($CFG->wwwroot . '/local/srl/vak/vak3.php');
          }
        ?>
       
      }
    </script>
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();

 ?>