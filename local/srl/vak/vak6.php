<?php

 require_once(__DIR__ . '/../../../config.php');
 $PAGE->set_url(new moodle_url('/local/srl/vak/vak6.php'));
 $PAGE->set_context(\context_system::instance());
 $PAGE->set_title('SRL Class');

//  $tipe = null;
//  $desc ='Kamu termasuk pembelajar tipe X. Agar pembelajaran makin maksimal, nantinya ada rekomendasi strategi belajar untukmu.';

  $getInfo = "SELECT * FROM {local_user} p WHERE p.user_moodle_id = :userid";
  $paramInfo = array('userid' => $USER->id);
  $resultInfo = $DB->get_records_sql($getInfo, $paramInfo);

  $a = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 1";
  $b = array('userid' => $USER->id);
  $c = $DB->get_records_sql($a, $b);

  $d = count($c);

  $e = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 2";
  $f = array('userid' => $USER->id);
  $g = $DB->get_records_sql($e, $f);

  $h = count($g);

  $i = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 3";
  $j = array('userid' => $USER->id);
  $k = $DB->get_records_sql($i, $j);

  $l = count($k);

 

 echo $OUTPUT->header();

 echo $d . '<br>';
 echo $h . '<br>';
 echo $l . '<br>';
 echo (($d > $h) && ($d > $l)) . '<br>';
 echo (($h > $d) && ($h > $l)) . '<br>';
 echo (($l > $h) && ($l > $d)) . '<br>';
 echo ((25 > 0) && (25 > 0)) . '<br>';
 echo ((0 > 25) && (0 > 25)) . '<br>';
 echo json_encode($resultInfo);

 ?>

<!doctype html>
 <html lang="en">
   <head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
     <link rel="stylesheet" type="text/css" href="style.css">
     <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
     <link rel="preconnect" href="https://fonts.gstatic.com">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
     <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
 
     <title>Kuesioner Gaya Belajar VAK</title>
   </head>
   <body>
     <div class="container">
     <div class="row">
        <div class="card mt-3 w-100" style="border-width: 0px !important;">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb breadcrumb-arrow p-0">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li aria-current="page" class="breadcrumb-item active">Tes Gaya Belajar</li>
            </ol>
          </nav>
        </div>
      </div>
     <div class="row">
         <div class="card w-100 mt-3">
           <div class="card-body" id="card-body-nm">
           <h5 class="card-title text-center mt-4 mb-5">Tes Gaya Belajar</h5>
           <iframe name="vak" style="display:none;"></iframe>
           <form class="container-fluid px-5" method="post" target="vak">
             <table class="table table-borderless table-sm">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">26.</th>
                   <th scope="col" style="width: 96%;">Saya pikir bahwa saya dapat mengetahui apakah seseorang berbohong jika:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no26" id="26a" value="1">
                     <label class="form-check-label" for="flexRadioDefault1">A. mereka menghindari melihat Anda.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no26" id="26b" value="2">
                     <label class="form-check-label" for="26b">B. perubahan suara mereka..</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no26" id="26c" value="3">
                     <label class="form-check-label" for="26c">C. mereka menunjukkan hal aneh.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">27.</th>
                   <th scope="col" style="width: 96%;">Ketika saya bertemu dengan seorang teman lama:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no27" id="27a" value="1">
                     <label class="form-check-label" for="27a">A. Saya berkata "Senang bertemu denganmu!"</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no27" id="27b" value="2">
                     <label class="form-check-label" for="27b">B. Saya berkata "Senang mendengar kabar tentangmu!"
                     </label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no27" id="27c" value="3">
                     <label class="form-check-label" for="27c">C. Saya memberi mereka pelukan atau jabat tangan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%">28.</th>
                   <th scope="col" style="width: 96%;">Saya mengingat hal-hal yang terbaik dengan cara:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no28" id="28a" value="1">
                     <label class="form-check-label" for="28a">A. menulis catatan atau menyimpan rincian materi print-out.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no28" id="28b" value="2">
                     <label class="form-check-label" for="28b">B. mengatakan dengan suara keras atau mengulang-ulang kata-kata penting dan menghafal kata-kata kunci.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no28" id="28c" value="3">
                     <label class="form-check-label" for="28c">C. melakukan dan mempraktikkan kegiatan atau membayangkan bagaimana suatu hal dilakukan.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">29.</th>
                   <th scope="col" style="width: 96%;">Jika saya mengeluh tentang barang rusak yang sudah dibeli, saya paling nyaman:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no29" id="29a" value="1">
                     <label class="form-check-label" for="29a">A. menulis surat.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no29" id="29b" value="2">
                     <label class="form-check-label" for="29b">B. mengontak melalui telepon.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no29" id="29c" value="3">
                     <label class="form-check-label" for="29c">C. mengembalikan ke toko atau menyampaikannya ke kepala kantor.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <table class="table table-borderless table-sm mt-4">
               <thead class="table-light" style="background-color: whitesmoke;">
                 <tr>
                   <th scope="col" style="width: 4%;">30.</th>
                   <th scope="col" style="width: 96%;">Saya cenderung mengatakan:</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no30" id="30a" value="1">
                     <label class="form-check-label" for="30a">A. Saya paham apa yang Anda maksud.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no30" id="30b" value="2">
                     <label class="form-check-label" for="30b">B. Saya mendengar apa yang Anda katakan.</label>
                   </div></td>
                 </tr>
                 <tr>
                   <th scope="row"></th>
                   <td><div class="form-check">
                     <input class="form-check-input" type="radio" name="no30" id="30c" value="3">
                     <label class="form-check-label" for="30c">C. Saya tahu bagaimana Anda merasakannya.</label>
                   </div></td>
                 </tr>
               </tbody>
             </table>
             <div class="d-inline-flex w-100 mb-3 mt-4">
             <div scope="col" style="width: 63%;">
             <nav aria-label="Page navigation">
               <ul class="pagination justify-content-end">
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>1</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>2</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>3</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>4</button></li>
                 <li class="page-item"><button class="page-link" onclick='getAllAnswer();' type="submit" name="submit" disabled>5</button></li>
                 <li class="page-item"><button class="page-link active" href="#">6</button></li>
               </ul>
             </nav>
             </div>
             <div scope="col" style="width: 37%;">
             <button class="btn btn-primary float-right mt-1" data-toggle="modal" data-target="#ModalVAK" style="font-weight: bold;" id="selesaiVAK">SELESAI</button>
             <div class="modal fade" id="ModalVAK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalVAKLabel" aria-hidden="true" style="display:none;">
                  
                  <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
                  <div class="modal-content">
                      <div class="modal-body text-center">
                        <img src="img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                        <h6 class="modal-title mt-2" id="ModalVAKLabel" style="color: #EFA30A;">Tes VAK Telah Selesai</h6>
                        <div class="small mt-2">Kamu akan dikembalikan ke halaman awal. Agar pembelajaran makin maksimal, nantinya ada rekomendasi strategi belajar untukmu.</div>
                          <a href="../../../my/index.php" class="btn btn-primary btn-block mt-3" style="font-weight: bold;" id="getResult">SELESAI</a> 
                        </div>
                      </div>
                  </div>
                </div>
             
             </div>
           </div>
           </form>
       </div>
     </div>
     </div>
 
     <!-- Optional JavaScript-->
 
     
     <script type="text/javascript" src="js/bootstrap.min.js"></script>
     <script> 
        <?php 
          function debug_to_console($data) {
            $output = $data;
            if (is_array($output))
                $output = implode(',', $output);
        
            echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
          }
        ?>

        ($(document).on("click", "#selesaiVAK",function(){
          <?php 
            if(!empty($_POST ['no26']) && !empty($_POST ['no27']) && !empty($_POST ['no28']) && !empty($_POST ['no29']) && !empty($_POST ['no30'])) {
              $record1 = new stdClass();
              $record1->user_id = $USER->id;
              $record1->vak_quest_id = "26";
              $record1->vak_user_answer = $_POST['no26'];

              $DB->insert_record('local_vak_answer', $record1);

              $record2 = new stdClass();
              $record2->user_id = $USER->id;
              $record2->vak_quest_id = "27";
              $record2->vak_user_answer = $_POST['no27'];

              $DB->insert_record('local_vak_answer', $record2);

              $record3 = new stdClass();
              $record3->user_id = $USER->id;
              $record3->vak_quest_id = "28";
              $record3->vak_user_answer = $_POST['no28'];

              $DB->insert_record('local_vak_answer', $record3);

              $record4 = new stdClass();
              $record4->user_id = $USER->id;
              $record4->vak_quest_id = "29";
              $record4->vak_user_answer = $_POST['no29'];

              $DB->insert_record('local_vak_answer', $record4);

              $record5 = new stdClass();
              $record5->user_id = $USER->id;
              $record5->vak_quest_id = "30";
              $record5->vak_user_answer = $_POST['no30'];

              $DB->insert_record('local_vak_answer', $record5);

              //get info local user by userid
              $getInfo = "SELECT * FROM {local_user} p WHERE p.user_moodle_id = :userid";
              $paramInfo = array('userid' => $USER->id);
              $resultInfo = $DB->get_records_sql($getInfo, $paramInfo);

              // $visual = 0;
              // $auditori = 2;
              // $kinestetik = 4;

              $getVisual = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 1";
              $paramVisual = array('userid' => $USER->id);
              $resultVisual = $DB->get_records_sql($getVisual, $paramVisual);

              $visual = count($resultVisual);

              $getAuditori = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 2";
              $paramAuditori = array('userid' => $USER->id);
              $resultAuditori = $DB->get_records_sql($getAuditori, $paramAuditori);

              $auditori = count($resultAuditori);

              $getKinestetik = "SELECT * FROM {local_vak_answer} p WHERE p.user_id = :userid  AND p.vak_user_answer = 3";
              $paramKinestetik = array('userid' => $USER->id);
              $resultkinestetik = $DB->get_records_sql($getKinestetik, $paramKinestetik);

              $kinestetik = count($resultKinestetik);

              $DB->delete_records('local_user',array('user_moodle_id' => $USER->id));

              // $getResultVisual = $DB->get_records_sql("SELECT * FROM {local_vak_answer} WHERE user_id = $USER->$id AND vak_user_answer = 1");
              // $getResultAuditori = $DB->get_records_sql("SELECT * FROM {local_vak_answer} WHERE user_id = $USER->$id AND vak_user_answer = 2");
              // $getResultKinestetik = $DB->get_records_sql("SELECT * FROM {local_vak_answer} WHERE user_id = $USER->$id AND vak_user_answer = 3");

              debug_to_console($visual);
              
              // $visual = count($getResultVisual);
              // $auditori = count($getResultAuditori);
              // $kinestetik = count($getResultKinestetik);

              if (($visual > $auditori) && ($visual > $kinestetik)) {
                $vakResult = 'VISUAL';
              } elseif (($auditori > $visual) && ($auditori > $kinestetik)) {
                $vakResult = 'AUDITORI';
              } elseif (($kinestetik > $visual) && ($kinestetik > $auditori)) {
                $vakResult = 'KINESTETIK';
              } else {
                $vakResult = 'VISUAL';
              }

              $result = new stdClass();
              $result->user_moodle_id = $USER->id;
              $result->name = $USER->username;
              $result->result_vak = $vakResult;
              $DB->insert_record('local_user', $result);

              // if($resultInfo == null) {
              //   $result = new stdClass();
              //   $result->user_moodle_id = $USER->id;
              //   $result->name = $USER->username;
              //   $result->result_vak = $vakResult;
              //   $DB->insert_record('local_user', $result);

              // } else {
              //   $data = reset($resultInfo)->user_id;
              //   $result = new stdClass();
              //   $result->user_id = $data->id;
              //   $result->user_moodle_id = $USER->id;
              //   $result->name = $USER->username;
              //   $result->result_vak = $vakResult;
              //   $DB->update_record('local_user', $result);
              // }

              // $DB->delete_records('local_vak_answer',array('user_id' => $USER->id));

              
            }
            
        ?>
        
        $('#ModalVAK').fadeIn();
        }))

        

       </script>

       
   </body>
 </html>

 <?php

 echo $OUTPUT->footer();

 ?>