<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form for editing HTML block instances.
 *
 * @package   block_srlblock
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_srlblock extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_srlblock');
    }

    function get_content() {
        global $DB;
        global $SESSION;
        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;


        $classes = $DB->get_records('local_srl_class');
        $classstring = '';
        foreach($classes as $class) {
            $classstring .=  '<html>
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" type="text/css" href="../blocks/srlblock/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="../blocks/srlblock/style.css">
            <link rel="stylesheet" type="text/css" href="../blocks/srlblock/fontawesome/css/all.min.css">
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
            
            
        </head>
        <body>
            <div class="container-sm mt-4 mb-3" >
                    <div class="card" style="border-width: 0px;">   
                        <img src="../blocks/srlblock/ASD3.png" class="card-img-top" alt="Algoritme dan Struktur Data">
                        <div class="card-body text-center">
                            <h6 class="card-text-nt">' . $class->class_name . '</h6>
                            <p class="small m-1">' . $class->lecturer . '</p>
                            <p class="small m-1">' . $class->year. '</p>
                            <a href="../local/srl/srlclass?id=' . $class->class_id . '" class="stretched-link"></a>
                        </div>
                    </div>
                </div>
            
        </body>

        <script>
        <?php
            if(!isempty($_POST["classid"])) {
                $SESSION->class_id = $_POST["classid"];
            }
        ?>
        </script>
        <br>
        </html>' ;

        }
        $this->content->text = $classstring; 

        
        return $this->content;
    }
}
