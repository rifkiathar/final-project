<?php

require_once(__DIR__ . '/../../config.php');

global $DB;

$PAGE->set_url(new moodle_url('/blocks/srladmin/delete.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Deleting');

$id = $_GET['id'];

$DB->delete_records('local_srl_class',array('class_id' => $id));

redirect($CFG->wwwroot . '/my');