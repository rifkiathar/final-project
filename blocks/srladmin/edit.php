<?php

require_once(__DIR__ . '/../../config.php');

global $DB;

$id = $_GET['id'];

$PAGE->set_url(new moodle_url('/blocks/srladmin/edit.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Editing');

$folderUploadGambar = "./assets/uploads/";

date_default_timezone_set('Asia/Jakarta');

$date = date("YmdHis", time());

# periksa apakah folder tersedia
if (!is_dir($folderUploadGambar)) {
  # jika tidak maka folder harus dibuat terlebih dahulu
  mkdir($folderUploadGambar, 0777, $rekursif = true);
}

$nama_mk = $_POST['nama_mk'];
$dosen_mk = $_POST['dosen_mk'];
$desc_mk = $_POST['desc_mk'];
$tahun_mk = $_POST['tahun_mk'];

# simpan masing-masing file ke dalam array 
# dan casting menjadi objek
$fileGambar = (object) @$_FILES['gambar_mk'];

// if ($fileFoto->size > 1000 * 2000) {
//   die("File tidak boleh lebih dari 1MB");
// }

// if ($fileGambar->type !== 'image/jpeg') {
//   die("File ktp harus jpeg!");
// }

# mulai upload file

$uploadGambarSukses = move_uploaded_file(
  $fileGambar->tmp_name, "{$folderUploadGambar}/{$fileGambar->name}"
);

$getClass = "SELECT * FROM {local_srl_class} p WHERE p.class_id = :userid";
$paramClass = array('userid' => $id);


$cek = new stdClass();
$cek->id = $id;
$cek->class_id = $id;
$cek->class_name = $nama_mk;
$cek->lecturer = $dosen_mk;
$cek->code = $desc_mk;
$cek->year = $tahun_mk;
$cek->date = $date;
$cek->image = $fileGambar->name;

$DB->update_record('local_srl_class', $cek);

echo json_encode($cek);

redirect($CFG->wwwroot . '/my');