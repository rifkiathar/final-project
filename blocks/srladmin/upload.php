<?php

require_once(__DIR__ . '/../../config.php');

global $DB;

$PAGE->set_url(new moodle_url('/blocks/srladmin/upload.php'));
$PAGE->set_context(\context_system::instance());
$PAGE->set_title('Uploading');

$folderUploadGambar = "./assets/uploads/";

date_default_timezone_set('Asia/Jakarta');

$date = date("YmdHis", time());

# periksa apakah folder tersedia
if (!is_dir($folderUploadGambar)) {
  # jika tidak maka folder harus dibuat terlebih dahulu
  mkdir($folderUploadGambar, 0777, $rekursif = true);
}

$nama_mk = $_POST['nama_mk'];
$dosen_mk = $_POST['dosen_mk'];
$desc_mk = $_POST['desc_mk'];
$tahun_mk = $_POST['tahun_mk'];

# simpan masing-masing file ke dalam array 
# dan casting menjadi objek
$fileGambar = (object) @$_FILES['gambar_mk'];

// if ($fileFoto->size > 1000 * 2000) {
//   die("File tidak boleh lebih dari 1MB");
// }

// if ($fileGambar->type !== 'image/jpeg') {
//   die("File ktp harus jpeg!");
// }

# mulai upload file

$uploadGambarSukses = move_uploaded_file(
  $fileGambar->tmp_name, "{$folderUploadGambar}/{$fileGambar->name}"
);

$cek = new stdClass();
$cek->class_name = $nama_mk;
$cek->lecturer = $dosen_mk;
$cek->code = $desc_mk;
$cek->year = $tahun_mk;
$cek->date = $date;
$cek->image = $fileGambar->name;

$DB->insert_record('local_srl_class', $cek);

redirect($CFG->wwwroot . '/my');


// if ($uploadFotoSukses) {
//   $link = "{$folderUpload}/{$fileFoto->name}";
//   echo "Sukses Upload Foto: <a href='{$link}'>{$fileFoto->name}</a>";
//   echo "<br>";
// }

// if ($uploadKtpSukses) {
//   $link = "{$folderUpload}/{$fileKtp->name}";
//   echo "Sukses Upload KTP: <a href='{$link}'>{$fileKtp->name}</a>";
//   echo "<br>";
// }