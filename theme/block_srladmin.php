<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Form for editing HTML block instances.
 *
 * @package   block_srladmin
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_srladmin extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_srladmin');
    }

    function get_content() {
        global $DB;
        if ($this->content !== NULL) {
            return $this->content;
        }

        // if(isset($_POST['submit'])){
        //   $recordtoinsert = new stdClass();
            
        //   $recordtoinsert->class_name = 'satu';
        //   $recordtoinsert->lecturer = 'dua';
        //   $recordtoinsert->desc = 'tiga';
        //   $recordtoinsert->year = 'empat';
        //   $recordtoinsert->date = 'lima';
        //   $recordtoinsert->image = 'enam';
        //   $DB->insert_record('local_srl_class', $recordtoinsert);
        // }

        $this->content = new stdClass;
        $classes = $DB->get_records('local_srl_class');
        $classstring = '';
        
        $classstring .= '<!doctype html>
        <html lang="en">
          <head>
            <!-- Required meta tags -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
            <title>Administrasi Mata Kuliah Aktif</title>
            <!-- Bootstrap CSS -->
            <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="style.css">
            <link rel="stylesheet" type="text/css" href="fontawesome/css/all.min.css">
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;1,100;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
        
          </head>
          <body>
          <a href="#" class="float"><i class="far fa-plus-square fa-lg" style="color: #28527A;" data-toggle="modal" data-toggle="tooltip" data-target="#ModalAddMK" data-placement="top" title="Tambah"></i></a>
          <br/>
          </h5>
          <div class="modal fade" id="ModalAddMK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalAddMKLabel" aria-hidden="true">
            <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
            <form class="modal-content" action="../blocks/srladmin/upload.php" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <h5 class="modal-title mt-2 mb-3 text-center" id="ModalAddMKLabel" style="color: #EFA30A;">Tambah Mata Kuliah</h5>
              <form>
                  <div class="row mb-3">
                    <label for="inputNamaMK" class="col-sm-2 col-form-label">Nama Mata Kuliah</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputNamaMK" name="nama_mk">
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="inputNamaDosen" class="col-sm-2 col-form-label">Nama Pengajar</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputNamaMK" name="dosen_mk">
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="TextareaDescMK" class="col-sm-2 form-label">Deskripsi</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="TextareaDescMK" rows="2" name="desc_mk"></textarea>
                    </div>
                  </div>
                  <div class="row mb-3">
                    <label for="TextareaTahunMK" class="col-sm-2 col-form-label">Tahun Ajaran</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="TextareaTahunMK" name="tahun_mk">
                  </div>
                </div>
                  <div class="row mb-3">
                    <label for="formFileMK" class="col-sm-2 form-label">Gambar Mata Kuliah</label>  
                    <div class="col-sm-10">
                      <input class="form-control" type="file" accept=".jpg,.gif,.png" id="formFileMK" name="gambar_mk">
                    </div>
                  </div>
                </form>
                <div class="d-grid text-right">
                  <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                  <button class="btn btn-primary" style="font-weight: bold;width: 100px;" id="tambahKelas">SIMPAN</button> 
                </div>
              </div>
              </form>
            </div>
          </div>';

        foreach($classes as $class) {
            $classstring .= '<div class="container-sm mt-4 mb-3">
            <div class="card" style="border-width: 0px;">   
            <img src="../blocks/srlblock/ASD3.png" class="card-img-top" alt="Algoritme dan Struktur Data">
            <div class="card-body">
            <div class="text-center" style="transform: rotate(0);">
              <h6 class="card-text-nt">' . $class->class_name . '</h6>
              <p class="small m-1">' . $class->lecturer . '</p>
              <p class="small m-1">' . $class->year . '</p>
              <a href="../local/srl/srlclass/admin.php?id=' . $class->class_id . '" class="stretched-link"></a>
            </div>
            <div class="text-center mt-3">
              <button type="button" class="btn btn-danger mr-2" data-toggle="modal" data-target="#ModalHapusMK" style="font-weight: bold;width: 95px;">HAPUS</button>
              <div class="modal fade" id="ModalHapusMK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalHapusMKLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document" style="width: 250px;">
              <div class="modal-content">
                <form class="modal-body text-center" action="../blocks/srladmin/delete.php?id='. $class->class_id.'" method="POST">
                  <img src="../local/srl/srlclass/img/ic_virtual tutor.png"class="flex-shrink-0 mr-2" width="200px" height="200px">
                  <h6 class="modal-title mt-2" id="ModalHapusMKLabel">Apakah kamu yakin akan menghapus kelas ini?</h6>
                  <div class="d-grid mt-3">
                    <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">TIDAK</button>
                    <button type="submit" class="btn btn-primary" style="font-weight: bold;width: 100px;">YA</button> 
                  </div>
                </form>
              </div>
            </div>
            </div>
              <a href="#" class="btn btn-primary" style="font-weight: bold;width: 95px;" data-toggle="modal" data-target="#ModalEditMK">EDIT</a>
              <div class="modal fade" id="ModalEditMK" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="ModalEditMKLabel" aria-hidden="true">
                <div class="modal-xl modal-dialog modal-dialog-centered" role="document">
                <form class="modal-content" action="../blocks/srladmin/edit.php?id='. $class->class_id.'" method="POST" enctype="multipart/form-data">
                  <div class="modal-body text-left">
                    <h5 class="modal-title mt-2 mb-3 text-center" id="ModalEditMKLabel" style="color: #EFA30A;">Edit Mata Kuliah</h5>
                    <form>
                      <div class="row mb-3">
                        <label for="inputEditNamaMK" class="col-sm-2 col-form-label">Nama Mata Kuliah</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEditNamaMK" name="nama_mk">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="inputEditNamaDosen" class="col-sm-2 col-form-label">Nama Pengajar</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputEditNamaDosen" name="dosen_mk">
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="TextareaEditDescMK" class="col-sm-2 form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="TextareaEditDescMK" rows="2" name="desc_mk"></textarea>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="TextareaEditTahunMK" class="col-sm-2 form-label">Tahun Ajaran</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="TextareaEditTahunMK" rows="2" name="tahun_mk"></textarea>
                        </div>
                      </div>
                      <div class="row mb-3">
                        <label for="formEditFileMK" class="col-sm-2 form-label">Gambar Mata Kuliah</label>  
                        <div class="col-sm-10">
                          <input class="form-control" type="file" accept=".jpg,.gif,.png" id="formEditFileMK" name="gambar_mk">
                        </div>
                      </div>
                    </form>
                    <div class="d-grid text-right">
                      <button type="button" class="btn btn-secondary mr-1" data-dismiss="modal" aria-label="Close" style="font-weight: bold;width: 100px;">BATAL</button>
                      <button href="#" class="btn btn-primary" style="font-weight: bold;width: 100px;">SIMPAN</button> 
                    </div>
                  </div>
                  </form>
                </div>
              </div> 
            </div>
            </div>
            </div>
          </div>';
        } 

        $classstring .= '<!-- Optional JavaScript; choose one of the two! -->
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        
      </body>
    </html>';

        $this->content->text = $classstring ;
        return $this->content;
    }
}